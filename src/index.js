import React from 'react';
import ReactDOM from 'react-dom';
import { HermesApp } from './HermesApp';
import 'animate.css';

ReactDOM.render(

  <HermesApp />,
  document.getElementById('root')
);

