import React from 'react';

export const Input = ({
    type, 
    name, 
    label,
    placeholder, 
    value, 
    onChange, 
    required,
    addedClass = ""

}) => {

    return (
        <div className="input-group mb-3">
            <span className="input-group-text  hidden" >{ label }</span>
            <input 
                type = { type }
                name = { name }
                placeholder={ placeholder } 
                value = { value }
                onChange = { onChange }
                className= { `form-control ${addedClass}` }
                required = { required}
                autoComplete = "off"
            />
        </div>
    )
}
