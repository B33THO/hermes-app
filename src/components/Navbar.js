import React from 'react';
import { FiBox, FiGrid, FiHome, FiShoppingCart, FiUsers, FiMenu, FiTruck } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import logo from '../images/logo.png'
import '../styles/styles.scss';

export const Navbar = () => {
    return (
        <>
            <nav className = "nav">

                <div className = "nav__container">

                    <div className="logo">
                    <img src={logo} className="img__logo" alt="img"/>
                    <h1 className="nav__logo">Hermes App</h1>
                    </div>

                    <label htmlFor="menu" className="nav__label">

                        <FiMenu className="nav__img" size={42}/>
                        {/*
                        scale for icon size
                        1x - 14px
                        2x - 28px
                        3x - 42px
                        4x - 56px
                        5x - 70px*/}

                    </label>

                    <input type="checkbox" id="menu" className="nav__input"/>

                    <div className="nav__menu">
                    
                        <Link to = '/hermesaplication' className = "nav__item"><FiHome size={28}/> Inicio</Link>               

                        <Link to = '/products' className = "nav__item"><FiBox size={28}/> Productos</Link> 

                        <Link to = '/orders' className = "nav__item"><FiTruck size={28}/> Pedidos</Link>

                        <Link to = '/purchases' className = "nav__item"><FiShoppingCart size={28}/> Compras</Link>

                        <Link to = '/providers' className = "nav__item"><FiUsers size={28}/> Proveedores</Link>                    
                        
                        <Link to = '/products' className = "nav__item"><FiGrid size={28}/> Inventario</Link>
                    
                    </div>

                </div>
            </nav>
        </>
    )
}
