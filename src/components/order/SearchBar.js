import React, { useState } from 'react';

export const SearchBar = () => {

  const [product, setProduct] = useState('');  
  return (
      <>
        <input 
            className = "form-control"
            placeholder='Ingrese su busqueda'
            name='product'
            value={product}
            onChange={ e => setProduct(e.target.value) }
        />
      </>
  );
};
