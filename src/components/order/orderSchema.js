
//#region 

/**
 * ETADOS DEL PEDIDO
 * 
 * 1. Capturado: Se almacenan el pedido en la base de datos.
 * 
 * 2. Pedido: Indica que la orden de compra ya fue enviada al proveedor
 * 
 * 3. Recibido: Indica que el producto a sido recibido pero no se a procesado, es decir,
 *              no se a actualizado el inventario y costos,
 * 
 * 4. Procesado: Los productos se han actualizado en el sistema. Los faltantes 
 *               se agregar al documento backorder.
 * 
 * 5. Finalizado: Indica que el pedido esta concluido                         
 */

/**
 * 
 * Folio: PD001236
 * Fecha: 12/01/2022
 * Proveedor: Truper Sa de Cv (_id: as12df16as1f3as)
 * Total Estimado: 8536.25
 * Status: CAPTURADO, PEDIDO, RECIBIDO, PROCESADO
 * Facturas: [
 *      {
 *          _id: 123a5wsd41565a4sd
 *          folio: A132645    
 *      }
 * ]
 * Productos: [
 *      {
 *          _id: 12356asas4fg3a,
 *          cantidad: 20,
 *          costo: 2
 *          total: 40
 *      },
 * 
 *      {
 *          _id: 12356asas4fg3a,
 *          cantidad: 30,
 *          costo: 5
 *          total: 150
 *      }
 * ]
 * 
 */

//#endregion

export const OrderSchema = {
    folio: '',
    orderDate: '',
    provider: '',
    estimatedTotal: 0,
    status: '',
    invoices: [],
    products: []
}

export const productDetails = {
    _id: '',
    quantity: 0,
    cost: 0,
    total: 0,
}

export const orderStatus = {
    CAPTURADO: 'CAPTURADO',
    PEDIDO: 'PEDIDO',
    RECIBIDO: 'RECIBIDO',
    PROCESADO: 'PROCESADO',
    FINALIZADO: 'FINALIZADO'
}