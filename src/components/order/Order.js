import moment from 'moment';
import React, { useEffect } from 'react';
import { FiArrowLeft, FiArrowLeftCircle } from 'react-icons/fi';
import { useSelector } from 'react-redux';
import Swal from 'sweetalert2';
import { useForm } from '../../hooks/useForm';
import { Navbar } from '../Navbar';
import { ProviderSelect } from '../providerComponents/ProviderSelect';
import { OrderSchema, orderStatus } from './orderSchema';
import { SearchBar } from './SearchBar';
;


export const Order = () => {

    moment.locale();
    const today = moment();
    
    const {providers} = useSelector(state => state.providers);
    const [orderState, handleOrderChange, resetOrder, addOrderValue] = useForm(OrderSchema);
    const {provider} = orderState;

    useEffect( ()=> {
        addOrderValue( {orderDate: today.format('YYYY-MM-DD h:mm a')} );
    }, [provider]);


    
    

    const back = () => {
        Swal.fire({
            title: "Confirmación",
            text: "¿Está seguro que desea cambiar de proveedor? Esto borrará su pedido actual",
            icon: "question",
            showConfirmButton: true,
            showDenyButton: true

        }).then( result => {
            if(result.isConfirmed ){

                resetOrder();
            } 
        });
    }
    return (
        <>
            <Navbar/>

            <div className = "container mt-3">
                              

                {
                    provider === '' ?
                        <div className = "row">
                            <h3 className = "mb-3">Nuevo Pedido</h3>  
                            <div className = "col-9">
                                <ProviderSelect 
                                    data={ providers }
                                    value={ provider }
                                    handleChange={ handleOrderChange }
                                />
                            </div>

                            <div className = "col">
                                <input 
                                    type="text"
                                    className = "form-control text-center"
                                    name="captureDate"
                                    value = { today.format('DD-MM-YYYY h:mm a') }
                                    readOnly                            
                                />
                            </div>
                        </div>
                    : null
                }


                {/**Order Body */}
                {
                    provider !== '' ? 
                    <>
                        <div className = "row">
                            <div className = "col-1">
                                <h2 className = "text-center" onClick={back}> <FiArrowLeftCircle/></h2>
                            </div>
                            <div className = "col">
                                <h4 className = "align-middle">
                                    Pedido a: { providers.find( p => (p._id === provider) )['provider_name'] }
                                </h4>
                            </div>
                        </div>

                        <div className = "row mt-2">
                            <div className = "col">
                                <SearchBar />
                            </div>

                        </div>
                    </>
                    : <p className = "mt-4">Seleccione un proveedor para continuar</p>
                }
            </div>
        
        </>
    )
}
