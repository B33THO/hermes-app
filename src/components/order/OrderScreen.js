import React from 'react';
import { FiPlus } from 'react-icons/fi';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { StartgetProviders } from '../../Redux/actions/providerActions';
import { Navbar } from '../Navbar';

export const OrderScreen = () => {

    const dispatch = useDispatch();
    dispatch( StartgetProviders() );
    return (
        <>
            <Navbar />
            <div className = "container">
                <Link 
                    to='/orders/neworder'
                    className = "btn btn-primary mt-3"
                >
                    <FiPlus/>Nuevo Pedido
                </Link>
                
            </div>
        </>

    )
}
