import React from 'react'
import { useDispatch } from 'react-redux';
import { useForm } from '../hooks/useForm';
import { startLogin } from '../Redux/actions/authActions';

import userIcon from "../images/user.svg";
import "../styles/styles.scss";
import Transitions from "./utilities/Transitions";

export const LoginScreen = () => {

    const dispatch = useDispatch();
    const [authValues, handleChange] = useForm({user: 'hermesadmin', password: 'q1aw2se3dr4f'});
    

    const connect = (e) => {
        e.preventDefault();
        console.log(e);
        dispatch( startLogin(user, password) );
    }

    const { user, password } = authValues;

    return (
      <>
      <h1 className="titulo">Hermes Aplication </h1>

      <div className="container">

        <div className="row">
          <div className="col-md-8 transitions">
            <Transitions></Transitions>
          </div>

          <div className="col-md-4" >
            <h2 className="lbl-log">Inicio de Sesión</h2>            
            <img className="icon-img" src={userIcon} alt="usericon"/>

            <form onSubmit={connect}> {/**  F O R M */}

            <div className="form-group mb-3">
              <div className="form-label lbl-user">Usuario</div>
              <input
                className="form-control input-log mb-4"
                name="user"
                value={user}
                onChange={handleChange}
                type="text"
                placeholder="Ingrese Usuario..."
              />
            </div>

            <div className="form-group mb-3">
              <div className="form-label lbl-user">Password</div>
              <input
                className="form-control input-log"
                name="password"
                value={password}
                onChange={handleChange}
                type="password"
                placeholder="Password"
              />
            </div>

            <div className="d-grid">
              <button className="btn btn-primary btn-block btn-log" type="submit">
                Conectar
              </button>
            </div>

            </form>
          </div>

        </div>{/** End Row */}

      </div>
    </>
    )
}
