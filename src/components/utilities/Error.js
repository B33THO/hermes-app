import React from 'react';

export const Error = ({msg}) => {
    return (
        <div className="alert alert-warning">
            <p>{msg}</p>
        </div>
    )
}
