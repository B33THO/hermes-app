import React from 'react'
import '../../styles/styles.scss';
import truper from '../../images/truper.jpg';
import sayer from '../../images/sayer.jpg';
import anbec from '../../images/anbec.png';
import cerrajes from '../../images/cerrajes.jpg';

export const Transitions = () => {
    return (
        <div className="slider">
                  <ul>
                      <li>
                          <img src={sayer} alt="sayerimg"/>
                      </li>
                      <li>
                          <img src={truper} alt="truperimg"/>
                      </li>
                      <li>
                          <img src={cerrajes} alt="cerrajesimg"/>
                      </li>
                      <li>
                          <img src={anbec} alt="anbecimg"/>
                      </li>
                  </ul>
              </div>
    )
}

export default Transitions;
