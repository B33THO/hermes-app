import React from 'react'
import { useDispatch } from 'react-redux'
import { logout } from '../Redux/actions/authActions';
import { Navbar } from './Navbar';

export const MainScreen = () => {

    const dispatch = useDispatch();
    const discconnect = () => {
        localStorage.removeItem('token');
        dispatch( logout() );
    }
    
    return (
        <>
            <Navbar />
            
            <div className = "container">
                <h1>Main Screen</h1>
                <button className = "btn btn-danger" onClick={ discconnect }>
                    Desconectar
                </button>
            </div>
        </>
    )
}
