import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector  } from 'react-redux';
import Swal from 'sweetalert2';
import { useForm } from '../../hooks/useForm';
import { saveProvider } from '../../Redux/actions/providerActions';
import { Navbar } from '../Navbar';
import { Error } from '../utilities/Error';
import { providerAddress, providerEmail, providerGeneralData, providerPhone, providerBankData, providerSchema } from './newProviderSchema';
import * as FaIcons from 'react-icons/fi';
import { useHistory } from 'react-router';
import { Address } from './Address';
import { GeneralProviderData } from './GeneralProviderData';
import { BankDataForm } from './BankDataForm';
import { EmailManager } from './EmailManager';
import { PhoneManager } from './PhoneManager';
import { isAlfanumericValue, isClabe, isEmail, isNuericValue, isPhoneNumber, isRfc } from '../../helpers/regex';
import { isDuplicateBankAccount, isDuplicateCondition, isDuplicateEmailRecord, isDuplicatePhoneRecord, isEmptyBankValues, isEmptyEmailValues, isEmptyPhoneValues, isMinLegth } from '../../helpers/validations';
import { Conditions } from './Conditions';
import { getFocus } from '../utilities/utilities';



//-------------------------------------------------- C O M P O N E N T ----------------------------------------------
export const NewProvider = () => {
    
    const dispatch = useDispatch();
    const history = useHistory();
    const {logged} = useSelector(state => state.auth);    

    //#region ---Form Manager---
    //Local State for new provider------------------------------------------------------|
    //This state contain the complete provider object structure                       //|
    const [ newProvider, setNewProvider] = useState(providerSchema);                  //|
    //----------------------------------------------------------------------------------|

    //State Form Manager-------------------------------------------------------------------------------------------|
    const [email_values, handleEmailChange, resetEmail] = useForm( providerEmail );                              //|
    const [phone_values, handlePhoneChange, resetPhone] = useForm( providerPhone, 'UPPERCASE' );                 //|
    const [bankDataValues, handleBankDataChange, resetBankData] = useForm( providerBankData, 'UPPERCASE' );      //|
    const [conditionValue, handleConditionChange, resetCondition] = useForm({condition: ''}, 'UPPERCASE');       //|
                                                                                                                 //|
    const [generalData, handleDataChange] = useForm( providerGeneralData, 'UPPERCASE' );                         //|
    const [address, handleAddressChange] = useForm(providerAddress, 'UPPERCASE');                                //|
    //-------------------------------------------------------------------------------------------------------------|

    //Localy State: Arrays----------------------------------------------------------------------------|
    const [emailVector, setEmailVector] = useState([]);                                             //|
    const [phoneVector, setPhoneVector] = useState([]);                                             //|
    const [bankVector, setBankVector] = useState([]);                                               //|
    const [conditionsVector, setConditionsVector] = useState([]);                                   //|
    //------------------------------------------------------------------------------------------------|
    //#endregion

    useEffect(() => {
        setNewProvider({
            ...generalData,
            providerAddress: {...address},
            emails: emailVector,
            phonenumbers: phoneVector,
            banksData: bankVector,
            conditions: conditionsVector
        });
        
    }, [generalData, address, emailVector, phoneVector, bankVector, conditionsVector]);


    //#region ------------------------------ EMAIL FUNCTIONS -----------------------------------------------------|
    const addEmail = () => {

        if(isEmptyEmailValues(email_values)){
            Swal.fire({
                title: 'Error...',
                icon: 'error',
                text: 'Para registrar un correo llene todos los campos',                
            });
            return;
        }

        if(!isMinLegth(contact_email, 3)){
            Swal.fire({
                title: 'Error...',
                icon: 'error',
                text: 'El nombre de contacto debe tener al menos 3 caracteres',                
            });
            return;
        }

        if(!isEmail(email_address)){
            Swal.fire({
                title: 'Error...',
                icon: 'error',
                text: 'Correo no válido',                
            });
            return;
        }

        if(isDuplicateEmailRecord(emailVector, email_address)){
            Swal.fire({
                title: 'Error...',
                icon: 'error',
                text: 'El correo ya ha sido agregado previamente',   
            });
            return;
        }

        setEmailVector([...emailVector, email_values]);
        resetEmail();
    }

    const removeEmail = (e) => {
        const emailToDelete = e.target.getAttribute("name");
        setEmailVector(emailVector.filter(e => e.email_address !== emailToDelete));
    }
    //------------------------------------------------------------------------------------------------------------|
    //#endregion

    //#region ------------------------------ PHONE FUNCTIONS -----------------------------------------------------|
    
    const addPhone = () => {
        
        if(isEmptyPhoneValues(phone_values)){
            Swal.fire({
                title: 'Error...',
                icon: 'error',
                text: 'Los campos con * son obligatorios',                
            });
            return;
        }

        if(!isMinLegth(contact_phone, 3)){
            Swal.fire({
                title: 'Error...',
                icon: 'error',
                text: 'El nombre de contacto debe tener al menos 3 caracteres',                
            });
            return;
        }

        if(isDuplicatePhoneRecord( phoneVector, phone_values)){
            Swal.fire({
                title: 'Error...',
                icon: 'error',
                text: 'El número telefónico y extensión ya han sido capturados previamente',                
            });
            return;
        }

        
        setPhoneVector([...phoneVector, phone_values]);
        resetPhone();
    }

    const removePhone = (e) => {
        const phoneToDelete = e.target.getAttribute("name");
        setPhoneVector( phoneVector.filter(p => p.phonenumber !== phoneToDelete));
    }
    //------------------------------------------------------------------------------------------------------------|
    //#endregion

    //#region ------------------------------ BANCK DATA FUNCTIONS ------------------------------------------------|
    const addBankData = () => { 
        
        if(isEmptyBankValues(bankDataValues)){
            Swal.fire({
                icon: 'error',
                title: 'Error...',
                text: 'Para agregar datos bancarios, llene todos los campos'
              });
            return;
        }

        if(!isNuericValue(number_account)){
            Swal.fire({
                icon: 'error',
                title: 'Error en el Número de Cuenta...',
                text: 'El número de cuenta solo debe contener dígitos'
              });
            return;
        }

        if(isDuplicateBankAccount(bankVector, number_account)){
            Swal.fire({
                icon: 'error',
                title: 'Error...',
                text: 'Número de cuenta agregado previamente'
              });
            return;
        }

        if(!isClabe(CLABE)){
            Swal.fire({
                icon: 'error',
                title: 'Error en la CLABE...',
                text: 'La CLABE debe contener 18 dígitos'
              });
            return;
        }

        if(!isAlfanumericValue(payment_reference)){
            Swal.fire({
                icon: 'error',
                title: 'Error en la CLABE...',
                text: 'La CLABE debe contener 18 dígitos'
              });
            alert('La referencia debe ser alfanumérica');
            return;
        }        
        
        setBankVector([...bankVector, bankDataValues])
        resetBankData();
    }

    const removeBankData = (e) => {
        const bankDataToRemove = e.target.getAttribute("name");
        setBankVector(bankVector.filter(b => b.number_account !== bankDataToRemove) );
    }
    //#endregion

    //#region ------------------------------ CONDITION FUNCTIONS -------------------------------------------------|
    const addCondition = () => {

        if( condition === ''){
            Swal.fire({
                icon: 'error',
                title: 'Error en Condiciones...',
                text: 'La descripción es requerida',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                  }
              });
            return;
        }
        
        if(isDuplicateCondition(conditionsVector, condition)){
            Swal.fire({
                icon: 'error',
                title: 'Error en Condiciones...',
                text: 'La descripción ya fue agregada previamente',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                  }
              });
            resetCondition();
            return;
        }
        setConditionsVector([...conditionsVector, condition]);
        resetCondition();
        getFocus('condition')
    }

    const removeCondition = (e) => {
        const conditiontoRemove = e.target.getAttribute("name");
        setConditionsVector( conditionsVector.filter( c => c !== conditiontoRemove));
    }
    //#endregion
    
       
    //#region -------------------------||| Principal fuction to save provider  |||--------------------------------|

    const saveNewProvider = newprov => dispatch( saveProvider(newprov, history));     


    const saveProviderSubmit = (e) => {
        e.preventDefault();

        if(!isRfc(rfc)){
            alert('El RFC es incorrecto');
            return;
        }

        if(Number(credit_days) < 0 ){
            alert('El valor días de crédito debe ser un número positivo o 0');
            return;
        }

        if(Number(credit_limit) < 0){
            alert('El valor del límite de crédito debe ser debe ser un número positivo o 0');
            return;
        }         
        
        Swal.fire({
            title: 'Desea guardar el proveedor?',
            icon: 'question',
            showDenyButton: true,
            showConfirmButton: true,
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
            }
        }).then( result => {
            if(result.isConfirmed){
                saveNewProvider(newProvider);
            } else if (result.isDenied) {
                Swal.fire('No se guardó el proveedor', '', 'info');
            }
        });
                
        
    }
    //#endregion

    //#region Destructure of Constants
    const { street, num_ext, num_int, suburb, location, reference, municipality, state, country, postalcode } = address;
    const { provider_name, rfc, credit_days, credit_limit } = generalData;
    const { contact_email, stall_email, email_address } = email_values;
    const { contact_phone, stall_phone, phonenumber, extension } = phone_values;
    const { bank_name, CLABE, number_account, payment_reference} = bankDataValues;
    const { condition } = conditionValue;
     //#endregion

    
    return (
    <>
        
        <Navbar />

        {
            logged ? 

            <div className = "container">
                <h2 className = "text-center">Nuevo Proveedor</h2>
                
                <form onSubmit = { saveProviderSubmit }>

                    {/* general data */}
                    <GeneralProviderData 
                        provider_name = { provider_name }
                        rfc = { rfc }
                        credit_days = { credit_days }            
                        credit_limit = { credit_limit }
                        handleDataChange = { handleDataChange }
                    />                

                    {/* -----ADDRESS----- */}                    
                        <Address 
                            street = { street }
                            num_ext = { num_ext }
                            num_int = { num_int }
                            suburb = { suburb }
                            location = { location }    
                            reference = { reference }
                            municipality = { municipality }
                            state = { state }
                            country = { country }    
                            postalcode = { postalcode }
                            handleAddressChange = { handleAddressChange }
                        />   

                    <div className="container-fluid grup">
                        {/* EMAILS */}
                        <EmailManager 
                            contact_email={ contact_email }
                            stall_email={ stall_email }
                            email_address={ email_address }
                            handleEmailChange={ handleEmailChange }
                            emailsList={ emailVector }
                            addEmail={ addEmail }
                            removeEmail={ removeEmail }
                        />
                    

                        {/* PHONE NUMBERS */}
                        <PhoneManager 
                            contact_phone={ contact_phone }
                            stall_phone={ stall_phone }
                            phonenumber={ phonenumber }
                            extension={ extension }
                            handlePhoneChange={ handlePhoneChange }
                            phoneList={ phoneVector }
                            addPhone={ addPhone }
                            removePhone={ removePhone }
                        />                
                    </div>

                    {/* BANK DATA */}    
                    <BankDataForm 
                        bank_name = { bank_name }
                        number_account = { number_account }
                        CLABE = { CLABE }
                        payment_reference = { payment_reference }
                        bankVector = { bankVector }
                        handleBankDataChange = { handleBankDataChange }
                        addBankData = { addBankData }
                        removeBankData = { removeBankData }

                    />                    
                    
                    {/* CONDITIONS */}
                    <Conditions 
                        condition={ condition }
                        conditionList={ conditionsVector }
                        handleConditionChange={ handleConditionChange }
                        addCondition={ addCondition }
                        removeCondition={ removeCondition }
                    />

                    <div className="col text-center mb-3">               
                        <button type = "submit" className="btn btn-primary"><FaIcons.FiSave/> Guardar Proveedor</button>
                    </div>
                </form>

            </div>
            : history.push('/')
        }
        
        </>
    )
}
