import React from 'react';
import * as FaIcons from 'react-icons/fi';

export const BankDataTable = ({bankVector , removeBankData}) => {
    return (
        <table className = "table table-striped mt-4">
            <thead>
                <tr>
                    <th>Banco</th>
                    <th>No. de Cuenta</th>
                    <th>CLABE</th>
                    <th>Referencia</th>
                </tr>
            </thead>

            <tbody>
                {                                
                    bankVector?

                    bankVector.map( bkd => (
                        
                        <tr key={bkd.number_account}>
                            <td>{bkd.bank_name}</td>
                            <td>{bkd.number_account}</td>
                            <td>{bkd.CLABE}</td>
                            <td>{bkd.payment_reference}</td>
                            <td>
                                <button
                                    name = {bkd.number_account}
                                    type = "button"
                                    onClick = { removeBankData }
                                    className="btn btn-danger rounded-circle">
                                    x
                                </button>
                            </td>
                        </tr>
                        
                    ))

                    :null
                }
            </tbody>
        </table>
    )
}
