import React from 'react';

export const GeneralProviderData = ({ 
    provider_name, rfc, credit_days, credit_limit, handleDataChange
}) => {

    document.addEventListener("wheel", e => {
        if(document.activeElement.type === "number"){
            document.activeElement.blur();
        }
    })
    return (
        <div className = "continer-fluid grup p-4 pt-3">
            
            <h4 className = "bg-primary rounded text-light text-center p-2 mb-3">
                Datos Principales
            </h4>

            {
                //#region Row 1
            }
            <div className = "row">
                
                {/* Provider name */}
                <div className="col">
                    <label className="form-label" form="provider_name">
                        Nombre del Proveedor* 
                    </label>

                    <input
                        className="form-control outline-0"
                        name="provider_name"
                        id="provider_name"
                        type="text"
                        autoComplete = "off"
                        required
                        value = { provider_name }
                        onChange = { handleDataChange }                    
                        
                    />
                </div>{/**---------------------------------------- */}

                
            </div>
            {
                //#endregion
            }
            

            {
                //#region Row 2
            }
            <div className = "row mt-4 justify-content-end">

                {/* ||||||  RFC  |||||| */} 
                <div className="col-sm-6">
                    <label className="form-label">
                        R.F.C* 
                    </label>

                    <input
                        className="form-control"
                        name="rfc"
                        id="rfc"
                        type="text"
                        placeholder = "R.F.C"
                        required
                        autoComplete = "off"
                        value = { rfc }
                        onChange = { handleDataChange }
                    />
                </div>{/**------------------------------------------- */}
                
                {/* |||||  Credit Days |||||*/}
                <div className="col-sm-3">
                    <label className="form-label">Días de Crédito </label>
                    <input
                        className="form-control  text-center"
                        name="credit_days"
                        id="credit_days"
                        type="number"
                        placeholder = "Días de Crédito"
                        required
                        value = {  credit_days  }
                        onChange = { handleDataChange }
                    />
                </div>{/**--------------------------------------------- */}

                {/* ||||| Límite de Crédito ||||| */}
                <div className="col-sm-3">
                    <label className="form-label">Límite de Crédito </label>
                    <input
                        className="form-control text-end"
                        name="credit_limit"
                        id="credit_limit"
                        type="number"
                        placeholder = "Límite de Crédito"
                        value = { credit_limit }
                        onChange = { handleDataChange }
                    />
                </div>{/**---------------------------------------------- */}
            </div>
            {
                //#endregion
            }
        </div>
    )
}
