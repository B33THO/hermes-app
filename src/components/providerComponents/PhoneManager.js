import React from 'react'

export const PhoneManager = ({ contact_phone, stall_phone, phonenumber, extension, handlePhoneChange, phoneList, addPhone, removePhone}) => {
    return (
        <>

        <h4 className = "bg-primary text-light rounded text-center p-2">Números Telefónicos</h4>
        
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Nombre *</th>
                        <th>Puesto *</th>
                        <th>Teléfono *</th>
                        <th>Extensión</th>
                        <th></th>                        
                    </tr>
                    <tr>
                        <td>
                            <input
                                className="form-control" 
                                type="text" 
                                name="contact_phone"
                                value = { contact_phone }
                                onChange = { handlePhoneChange }
                            />
                        </td>
                        <td>
                            <input 
                                className="form-control"
                                type="text" 
                                name="stall_phone"
                                value = { stall_phone }
                                onChange = { handlePhoneChange }
                            />
                        </td>
                        <td>
                            <input 
                                className="form-control"
                                type="text" 
                                name="phonenumber"
                                value = { phonenumber }
                                onChange = { handlePhoneChange }
                            />
                        </td>
                        <td>
                            <input 
                                className="form-control"
                                type="text" 
                                name="extension"
                                value = { extension }
                                onChange = { handlePhoneChange }
                            />
                        </td>
                        <td>
                            <button 
                                onClick = { addPhone } type="button" 
                                className="btn btn-success rounded-circle"
                            >
                                +
                            </button>
                            </td>
                    </tr>
                </thead>

                <tbody>
                    {
                        phoneList ?
                            phoneList.map( p => (
                                
                                <tr key={p.phonenumber +''+ p.extension}>
                                    <td>{p.contact_phone}</td>
                                    <td>{p.stall_phone}</td>
                                    <td>{p.phonenumber}</td>
                                    <td>{p.extension}</td>
                                    <td>
                                        <button 
                                            name = { p.phonenumber+''+p.extension } 
                                            onClick = { removePhone } 
                                            type="button"
                                             className="btn btn-danger rounded-circle">
                                            X
                                        </button>
                                    </td>
                                </tr>
                            ))
                        : null  
                    }
                </tbody>
            </table>
        </>
    )
}
