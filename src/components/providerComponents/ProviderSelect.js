import React from 'react';

export const ProviderSelect = ({data, value, handleChange, select = false}) => {    

    
    return (
        <select
            className = "form-select"
            name = "provider"
            value = {value.provider}
            onChange = { handleChange }
        > 
            <option value={''}>--Seleccione Provedor--</option>
            {
                data?
                    data.map( d => {
                        return (
                            <option 
                                key={d._id} 
                                value={d._id}
                                selected = { select === d._id}
                            >
                                {d.provider_name}
                            </option>
                        )
                    })
                :null
            }           
        </select>
    )
}

//Data: Provider list from database
