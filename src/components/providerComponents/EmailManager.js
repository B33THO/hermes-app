import React from 'react';

export const EmailManager = ({ contact_email, stall_email, email_address, handleEmailChange, emailsList, addEmail, removeEmail }) => {

    return (
        <>

            <h4 className = "bg-primary text-light rounded text-center p-2">Correos Electrónicos</h4>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>NOMBRE</th>
                        <th>PUESTO</th>
                        <th>CORREO</th>
                        <th></th>                        
                    </tr>
                    <tr>
                        <td>
                            <input 
                                className="form-control"
                                type="text" 
                                name="contact_email"
                                value = { contact_email }
                                onChange = { handleEmailChange }
                            />
                        </td>
                        <td>
                            <input 
                                className="form-control"
                                type="text" 
                                name="stall_email"
                                value = { stall_email }
                                onChange = { handleEmailChange }
                            />
                        </td>
                        <td>
                            <input 
                                className="form-control"
                                type = "email" 
                                name = "email_address"
                                value = { email_address }    
                                onChange = { handleEmailChange }                        
                            />
                        </td>
                        <td>
                            <button 
                                onClick = { addEmail } 
                                type="button" 
                                className="btn btn-success rounded-circle"
                            >
                                +
                            </button>
                        </td>
                    </tr>
                </thead>

                <tbody>
                    
                    {
                        emailsList ?
                            emailsList.map( em => (
                                
                                <tr key={em.email_address}>
                                    <td>{em.contact_email}</td>
                                    <td>{em.stall_email}</td>
                                    <td>{em.email_address}</td>
                                    <td>
                                        <button 
                                            type="button" 
                                            className="btn btn-danger rounded-circle"
                                            name= { em.email_address } 
                                            onClick = { removeEmail } 
                                        >
                                            X
                                        </button>
                                    </td>
                                </tr>
                            ))
                        : null  
                    }
                    
                </tbody>
            </table>
        </>
    )
}
