import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { selectProviderToEdit, setProvidertoRead, StartgetProviders } from '../../Redux/actions/providerActions';
import { Navbar } from '../Navbar';
import { FiEdit, FiEye, FiPlusCircle } from 'react-icons/fi';
import ReactLoading from 'react-loading';
import { openProviderReaderModal } from '../../Redux/actions/uiActions';
import { ProviderReaderModal } from './ProviderReaderModal';

export const ProviderScreen = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const {loading, providers, provider_read} = useSelector(state => state.providers);
    


    useEffect(() => {
        
        dispatch( StartgetProviders() );
        
    }, [dispatch]);


    const handleEditProvider = ( provider ) => {

        dispatch( selectProviderToEdit(provider) );
        history.push( '/provider/editprovider' );
    }

    const handleReadProvider = ( provider ) => {
        dispatch( setProvidertoRead(provider) );
        dispatch( openProviderReaderModal() );
    }

    
    return (
        <>
            <Navbar />
            <div className = "container">

                <h1>Proveedores</h1> 

                <Link to = '/provider/newprovider'
                    className = "btn btn-primary mb-4"
                > <FiPlusCircle/> Agregar Proveedor</Link> 

                {
                    loading? 
                    <div className = "container fs-6 text-center">
                        <ReactLoading 
                            type={'bubbles'}
                            color='#0059ff'
                            delay={10}
                            height={150}
                            width={150}
                        />

                    </div>

                    :

                    <div>
                        <table className = "table">
                            <thead>
                                <tr>
                                    <th>Proveedor</th>
                                    <th>R.F.C</th>
                                </tr>
                            </thead>

                            <tbody>
                                {
                                    providers?
                                        providers.map(
                                            provider => (
                                                <tr key = {provider._id}>
                                                    <td>{provider.provider_name}</td>
                                                    <td>{provider.rfc}</td>
                                                    
                                                    <td>
                                                        <button 
                                                            to="/provider/editprovider" 
                                                            className = "btn btn-warning rounded-circle me-3"
                                                            onClick = {()=>handleEditProvider(provider)}  
                                                        >
                                                            <FiEdit />
                                                        </button>                                                    

                                                        <button 
                                                            className = "btn btn-primary rounded-circle" 
                                                            type="button"
                                                            onClick={ ()=>handleReadProvider(provider) }
                                                        >
                                                            <FiEye />
                                                        </button>                                                
                                                    </td>                                           
                                                </tr>
                                            )
                                        )
                                    : null
                                }
                            </tbody>
                        </table>
                    </div>
                }      
                {
                    provider_read ?
                        <ProviderReaderModal />
                        : null
                }

            </div>
            
        </>
    )
}
