import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from '../../hooks/useForm';
import Swal from 'sweetalert2';
import { Navbar } from '../Navbar';
import { GeneralProviderData } from './GeneralProviderData';
import { Address } from './Address';
import { BankDataForm } from './BankDataForm';
import { EmailManager } from './EmailManager';
import { PhoneManager } from './PhoneManager';
import { Conditions } from './Conditions';
import { isDuplicateBankAccount, isDuplicateCondition, isDuplicateEmailRecord, isDuplicatePhoneRecord, isEmptyBankValues, isEmptyEmailValues, isEmptyPhoneValues } from '../../helpers/validations';
import { providerAddress, providerBankData, providerEmail, providerGeneralData, providerPhone, providerSchema } from './newProviderSchema'
import { isAlfanumericValue, isClabe, isEmail, isNuericValue, isPhoneNumber, isRfc } from '../../helpers/regex';
import { getFocus } from '../utilities/utilities';
import { startEditProvider } from '../../Redux/actions/providerActions';
import { useHistory } from 'react-router-dom';

export const EditProviderData = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const { provider_edit } = useSelector(state => state.providers);
    const [providerState, setProviderState] = useState(providerSchema);

    const [generalData, handleGeneralDataChange, resetData, addGeneralData] = useForm(providerGeneralData, 'UPPERCASE'); 
    const [address, handleAddressChange, resetAddresss, addAddress] = useForm(providerAddress, 'UPPERCASE');
    const [ email, handleEmailChange, resetEmail ] = useForm( providerEmail );
    const [ phone, handlePhoneChange, resetPhone ] = useForm( providerPhone, 'UPPERCASE' );
    const [ bankData, handleBankDataChange, resetBankData ] = useForm( providerBankData, 'UPPERCASE' );
    const [ conditions, handleConditionChange, resetCondition ] = useForm({condition:''}, 'UPPERCASE')

    const [emailsVector, setEmailVector ] = useState([]);    
    const [phonesVector, setPhoneVector ] = useState([]);
    const [bankDataVector, setBankDataVector] = useState([]);
    const [conditionVector, setConditionVector] = useState([]);

    //destructuring-------------------------------------------------------------------------------------------------|
    
    const { provider_name, rfc, credit_days, credit_limit } = generalData;

    const { street, num_ext, num_int, suburb, location, reference, municipality,
        state, country, postalcode
    } = address;

    const { contact_email, stall_email, email_address } = email;
    const { contact_phone, stall_phone, phonenumber, extension } = phone;
    const { bank_name, number_account, CLABE, payment_reference} = bankData;
    const { condition } = conditions;

    // Load Provider Data 
    useEffect( () => {

        setProviderState( provider_edit );
        
        addGeneralData( {
            provider_name: provider_edit.provider_name,
            rfc: provider_edit.rfc,
            credit_days: provider_edit.credit_days,
            credit_limit: provider_edit.credit_limit
        } );
        addAddress( {...provider_edit.providerAddress} );
        setEmailVector( provider_edit.emails );
        setPhoneVector( provider_edit.phonenumbers );
        setBankDataVector( provider_edit.banksData );
        setConditionVector( provider_edit.conditions );

    },[provider_edit]);

    //Update State
    useEffect(() => {
        
        setProviderState({
            ...providerState,
            ...generalData,
            providerAddress: address,
            emails: emailsVector,
            phonenumbers: phonesVector,
            banksData: bankDataVector,
            conditions: conditionVector
        });
    }, [generalData, address, email, phone, bankData, conditions] );

    //--------------------------------- EMAIL ----------------------------------------------------------|
    //#region email
    const addEmail = () => {

        if( isEmptyEmailValues(email) ){
            alert('Todos los campos son requeridos');
            return;
        }
        
        if( !isEmail(email_address) ){
            alert('el email es incorrecto');
            return;
        }

        if(isDuplicateEmailRecord(emailsVector, email_address)){
            alert('Email capturado previamente');
            return;
        }
        
        setEmailVector( [...emailsVector, email]);
        resetEmail();
    }

    const removeEmail = (e) => {
        const emailToDelete = e.target.getAttribute('name');
        setEmailVector( emailsVector.filter( e => e.email_address !== emailToDelete) );
    }
    //#endregion

    //--------------------------------- PHONES ---------------------------------------------------------|
    //#region Phone
    const addPhone = () => {

        console.log(phone);

        if( isEmptyPhoneValues(phone) ){
            alert('Nombre, puesto y teléfono son requeridos!');
            return;
        }
        
        if( !isPhoneNumber(phonenumber) ){

            alert('Número Telefónico no valido');
            return;
        }

        if( isDuplicatePhoneRecord(phonesVector, phonenumber) ){

            alert('Número telefónico capturado previamente');
            return;
        }

        setPhoneVector( [...phonesVector, phone]);
        resetPhone();
    }

    const removePhone = (e) => {
        
        console.log(e.target);
        const phoneToRemove = e.target.getAttribute('name');
        setPhoneVector( phonesVector.filter( p => p.phonenumber+''+p.extension !== phoneToRemove) );
    }
    //#endregion

    //--------------------------------- BANKS ----------------------------------------------------------|
    //#region  Banks
    const addBankData = () => {
        
        if(isEmptyBankValues(bankData)){
            Swal.fire({
                icon: 'error',
                title: 'Error...',
                text: 'Para agregar datos bancarios, llene todos los campos',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                  }
              });
            return;
        }

        if(!isNuericValue(number_account)){
            
            Swal.fire({
                icon: 'error',
                title: 'Error en el Número de Cuenta...',
                text: 'El número de cuenta solo debe contener dígitos',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                  }
              });
            return;
        }

        if(isDuplicateBankAccount(bankDataVector, number_account)){
            Swal.fire({
                icon: 'error',
                title: 'Error...',
                text: 'Número de cuenta agregado previamente',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                  }
              });
            return;
        }

        if(!isClabe(CLABE)){
            
            Swal.fire({
                icon: 'error',
                title: 'Error en la CLABE...',
                text: 'La CLABE debe contener 18 dígitos'
              });
            return;
        }

        if(!isAlfanumericValue(payment_reference)){
            Swal.fire({
                icon: 'error',
                title: 'Error en la CLABE...',
                text: 'La CLABE debe contener 18 dígitos',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                  }
              });
            alert('La referencia debe ser alfanumérica');
            return;
        } 
        
        setBankDataVector( [...bankDataVector, bankData] );
        resetBankData();
        
    }

    const removeBankData = (e) => {
        
        const bankToRemove = e.target.getAttribute('name');
        setBankDataVector( bankDataVector.filter( elem => elem.number_account !== bankToRemove ) );
        
    }
    //#endregion

    //--------------------------------- CONDITIONS------------------------------------------------------|
    //#region Conditions
    const addCondition = () => {
        
        if(condition === ''){
            Swal.fire({
                icon: 'error',
                title: 'Error en Condiciones...',
                text: 'La descripción es requerida',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                  }
              });
            return;
        }
        
        if(isDuplicateCondition(conditionVector, condition) ){
            Swal.fire({
                icon: 'error',
                title: 'Error en Condiciones...',
                text: 'La descripción ya fue agregada previamente',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                  }
              });
            resetCondition();
            return;
        }

        setConditionVector( [...conditionVector, condition] );
        resetCondition();
        getFocus('condition');
        
    }

    const removeCondition = (e) => {
        
        const conditionToRemove = e.target.getAttribute('name');
        console.log(conditionVector);
        setConditionVector(conditionVector.filter( c => c !== conditionToRemove ));
    }
    //#endregion


    //---|||||||||||||-----SAVE CHANGE FUNCTION-----|||||||||||||||-----
    //#region main function
    const handleSaveChangues = (e) => {

        e.preventDefault();

        Swal.fire({
            title: 'Desea guardar los cambios realizados?',
            icon: 'question',
            showConfirmButton: true,
            showDenyButton: true,
            showClass: {
              popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
              popup: 'animate__animated animate__fadeOutUp'
            }
          }).then( result => {
                if(result.isConfirmed){
                    //save changues
                    dispatch( startEditProvider(provider_edit._id, providerState, history) );

                } else if (result.isDenied) {
                    Swal.fire('Los cambios no se han guardado', '', 'info')
                }
          });
        
    }
    //#endregion
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    return (

        <>
            <Navbar />
            <div className = "container">

                <div className = " text-center mt-4">
                    <h2>Edición de Proveedor</h2>
                </div>

                <form onSubmit={handleSaveChangues}>

                    <GeneralProviderData 
                        provider_name = { provider_name }
                        rfc = { rfc }
                        credit_days={ credit_days }
                        credit_limit={ credit_limit }
                        handleDataChange={ handleGeneralDataChange }
                    /> 

                    <Address 
                        street={ street }
                        num_ext={ num_ext }
                        num_int={ num_int }
                        suburb={suburb}
                        location={ location }
                        reference={ reference }
                        municipality={ municipality }
                        state={ state }
                        country={ country }
                        postalcode={ postalcode }
                        handleAddressChange={ handleAddressChange }
                    />

                    <EmailManager 
                        contact_email={ contact_email }
                        stall_email={ stall_email }
                        email_address={ email_address }
                        handleEmailChange={ handleEmailChange }
                        emailsList={ emailsVector }
                        addEmail={ addEmail }
                        removeEmail={ removeEmail }
                    />

                    <PhoneManager 
                        contact_phone={ contact_phone}
                        stall_phone={ stall_phone }
                        phonenumber={ phonenumber }
                        extension={ extension }
                        handlePhoneChange={ handlePhoneChange }
                        phoneList={ phonesVector }
                        addPhone={ addPhone }
                        removePhone={ removePhone }
                    />

                    <BankDataForm 
                        bank_name={ bank_name }
                        number_account={ number_account }
                        CLABE={ CLABE }
                        payment_reference={ payment_reference }
                        bankVector={ bankDataVector }
                        handleBankDataChange={ handleBankDataChange }
                        addBankData={ addBankData }
                        removeBankData={ removeBankData }
                    />

                    <Conditions 
                        condition={ condition }
                        conditionList={ conditionVector }
                        handleConditionChange={ handleConditionChange }
                        addCondition={ addCondition }    
                        removeCondition={ removeCondition }        
                    />

                    <div className = "text-center">
                        <button
                            className = "btn btn-primary w-25 mb-4"
                            type='submit'
                        >
                            Actualizar Proveedor
                        </button>                       
                    </div>

                </form>
            </div>
        </>
    )
}
