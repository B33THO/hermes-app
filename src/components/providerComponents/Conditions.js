import React from 'react'

export const Conditions = ({condition, conditionList, handleConditionChange, addCondition, removeCondition}) => {
    
    const items = conditionList.length;
    let counter = 1;

    const itemColor = (index) => {
        counter++
        if(index % 2 === 0 ){
            return '';
        }else {
            return 'list-group-item-secondary'
        }
    }
    return (
        <>

            <div className = "container-fluid grup p-4">
                <h4 className = "text-center">Información Adicional</h4>

                <div className = "row">
                    <div className = "col-md-11">
                        <label>Descripción</label>
                        <input 
                            className = "form-control"
                            type="text"
                            autoComplete="off"
                            name="condition"
                            id="condition"
                            value={ condition }
                            onChange={ handleConditionChange }
                        />
                    </div>

                    <div className = "col-md-1">
                        <button
                            type="button"
                            className = "btn btn-success rounded-circle mt-4"
                            onClick={ addCondition }
                        >+</button>
                    </div>
                </div>

                <div>
                    <ol className = "list-group mt-4">
                        {
                            conditionList ?
                                conditionList.map( condition => (
                                    
                                    <li 
                                        key={condition}                                        
                                        className = {`list-group-item ${itemColor(counter)}`}
                                    >
                                        
                                        <div className = "row">
                                            <div className = "col">
                                                <p>{condition}</p>

                                            </div>
                                            <div className = "col-md-1">

                                                <button 
                                                    className = "btn btn-danger rounded-circle"
                                                    name={ condition }
                                                    onClick={ removeCondition }
                                                >x</button>
                                            </div>
                                        </div>
                                        
                                        
                                    </li>
                                ))
                            :null
                        }
                    </ol>
                </div>
            </div>   
        </>
    )
}
