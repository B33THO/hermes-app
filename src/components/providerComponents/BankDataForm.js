import React from 'react';
import { BankDataTable } from './BankDataTable';

export const BankDataForm = ({ 
    bank_name, number_account, CLABE, payment_reference, bankVector,
    handleBankDataChange, addBankData, removeBankData
}) => {
    return (
        <div className = "container-fluid grup mt-4 p-4">

            <h4 className = "bg-primary rounded text-light text-center p-2">
                Datos Bancarios
            </h4>
            
            <div className = "row">
                {/**Bank name */}
                <div className ="col">
                    <label className="form-label">Banco </label>
                    <input
                        className="form-control"
                        name="bank_name"
                        type="text"
                        placeholder = "Banco"
                        value = { bank_name }
                        onChange = { handleBankDataChange }
                    />
                </div>
            </div>

            <div className = "row mt-4">

                {/**Number Account */}
                <div className ="col-3">
                    <label className="form-label">No. de Cuenta </label>
                    <input
                        className="form-control"
                        name="number_account"
                        type="text"
                        placeholder = "No. de Cuenta"
                        value = { number_account }
                        onChange = { handleBankDataChange }
                    />
                </div>

                {/** CLABE */}
                <div className ="col-4">
                    <label className="form-label">CLABE </label>
                    <input
                        className="form-control"
                        name="CLABE"
                        type="text"
                        placeholder = "CLABE"
                        value = { CLABE }
                        onChange = { handleBankDataChange }
                    />
                </div>

                {/** Bank reference */}
                <div className ="col-3">
                    <label className="form-label">Referencia </label>
                    <input
                        className="form-control"
                        name="payment_reference"
                        type="text"
                        placeholder = "Referencia"
                        value = { payment_reference }
                        onChange = { handleBankDataChange }
                    />
                </div>

                {/** Button Add */}
                <div className = "col-2 ">
                    <button 
                        onClick={ addBankData } 
                        type="button" 
                        className="btn btn-success rounded-circle mt-4"
                    >
                        +
                    </button>
                </div>

                <BankDataTable 
                    bankVector = { bankVector }
                    removeBankData = { removeBankData }
                />
            </div>
        </div>
    )
}
