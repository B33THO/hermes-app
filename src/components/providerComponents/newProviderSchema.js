export const providerAddress = {
    
    street: '',
    num_ext: '',
    num_int: '',
    suburb: '',
    location: '',
    reference: '',
    municipality: '',
    state: '',
    country: '',
    postalcode: 0
    
}

export const providerGeneralData = {
    provider_name: '',
    rfc: '',
    credit_days: 0,
    credit_limit: 0,
    active: true,
}

export const providerBankData = {    
        bank_name: '',
        CLABE: '',
        number_account: '',
        payment_reference: ''    
}

export const providerEmail = {
    contact_email:'', 
    stall_email:'', 
    email_address:''
}

export const providerPhone = {
    contact_phone:'', 
    stall_phone:'', 
    phonenumber:'',
    extension: ''
}

export const providerSchema = {
    provider_name: '',
    rfc: '',
    credit_days: 0,
    credit_limit: 0,
    active: true,    
    providerAddress,
    emails: [],
    phonenumbers: [],
    banksData: [],
    conditions: [],
    isSaved: false
}

