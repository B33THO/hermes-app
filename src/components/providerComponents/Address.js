import React from 'react'

export const Address = ({
    street, num_ext, num_int, suburb, location, reference, municipality,
    state, country, postalcode,
    handleAddressChange
}) => {

    return (
        <div className = "container-fluid grup direction p-4">
            <h4 className = "bg-primary rounded text-light text-center p-2 ">
                Dirección
            </h4>

            {
                //#region Row 1
            }
            <div className = "row">
                
                {/* Calle */}
                <div className="col-sm-8">
                    <label className="form-label">Calle </label>
                    <input
                        className="form-control"
                        name="street"
                        type="text"
                        placeholder = "Calle"
                        value = { street }
                        onChange = { handleAddressChange }
                    />
                </div>

                {/* Exterior Number */}
                <div className="col-sm-2">
                    <label className="form-label">No. Ext </label>
                    <input
                        className="form-control  text-center"
                        name="num_ext"
                        type="text"
                        placeholder = "No. Ext"
                        value = { num_ext }
                        onChange = { handleAddressChange }
                    />
                </div>

                {/* Interiron Number */}
                <div className="col-sm-2">
                    <label className="form-label">No. Int </label>
                    <input
                        className="form-control  text-center"
                        name="num_int"
                        type="text"
                        placeholder = "No. Int"
                        value = { num_int }
                        onChange = { handleAddressChange }
                    />
                    </div>

            </div> 
            {
                //#endregion
            }

            {
                //#region Row 2
            }
            
            <div className = "row mt-4"> 
                {/* Colony */}
                <div className="col">
                    <label className="form-label">Colonia </label>
                    <input
                        className="form-control"
                        name="suburb"
                        type="text"
                        placeholder = "Colonia"
                        value = { suburb }
                        onChange = { handleAddressChange }
                    />
                </div>

                {/* Locality */}
                <div className="col">
                    <label className="form-label">Localidad </label>
                    <input
                        className="form-control"
                        name="location"
                        type="text"
                        placeholder = "Localidad"
                        value = { location }
                        onChange = { handleAddressChange }
                    />
                </div>

                
                {/* Reference */}
                <div className="col-6">
                    <label className="form-label">Referencia </label>
                    <input
                        className="form-control"
                        name="reference"
                        type="text"
                        placeholder = "Referencia"
                        value = { reference }
                        onChange = { handleAddressChange }
                    />
                </div>

            </div>
            {
                //#endregion
            }

            {
                //#region Row 3
            }

            <div className = "row mt-4">

                {/* Municipality */}
                <div className="col-sm-3">
                    <label className="form-label">Municipio </label>
                    <input
                        className="form-control"
                        name="municipality"
                        type="text"
                        placeholder = "Municipio"
                        value = { municipality }
                        onChange = { handleAddressChange }
                    />
                </div>

                {/* state */}
                <div className="col-sm-3">
                    <label className="form-label">Estado </label>
                    <input
                        className="form-control"
                        name="state"
                        type="text"
                        placeholder = "Estado"
                        value = { state }
                        onChange = { handleAddressChange }
                    />
                </div>

                 {/* Country */}
                <div className="col-sm-3">
                    <label className="form-label">Pais </label>
                    <input
                        className="form-control"
                        name="country"
                        type="text"
                        placeholder = "Pais"
                        value = { country }
                        onChange = { handleAddressChange }
                    />
                </div>

                {/* Postal Code */}
                <div className="col-sm-3">
                    <label className="form-label">C.P </label>
                    <input
                        className="form-control"
                        name="postalcode"
                        type="number"
                        placeholder = "C.P."
                        value = { postalcode }
                        onChange = { handleAddressChange }
                    />
                </div>

            </div>

            {
                //#endregion
            }

        </div>
    )
}
