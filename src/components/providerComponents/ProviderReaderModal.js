import React from 'react';
import Modal from 'react-modal'
import { useDispatch, useSelector } from 'react-redux';
import { closeProviderReaderModal } from '../../Redux/actions/uiActions';

const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)'
    },
  };

Modal.setAppElement('#root');

export const ProviderReaderModal = () => {

    const dispatch = useDispatch();
    const { providerReaderModal } = useSelector(state => state.ui);
    const { provider_read } = useSelector(state => state.providers);

    const { provider_name, rfc, credit_days, credit_limit } = provider_read;
    const { street, num_ext, num_int, suburb, location, reference, municipality, state, country, postalcode} = provider_read.providerAddress;


    const closeModal = () => {
        dispatch( closeProviderReaderModal() );
    }
    
    return (
        <Modal
            isOpen={ providerReaderModal }
            onRequestClose = { closeModal }
            style={ customStyles }
        >
            {
                provider_read ?
            
                    <div className = "container">

                        <div className = "providerreader__container">
                            
                            <div className=" gc_cell_1-7 provider pt-2 text-center">
                                {provider_name}
                            </div>

                            <div className="gc_cell_1-4">
                                <p className = "providerreader__label">R.F.C.</p>
                                <p className = "providerreader__content">{rfc}</p>              
                            </div>  

                            <div className="gc_cell_4-5">
                                <p className = "providerreader__label">Días de Crédito</p>
                                <p className = "providerreader__content text-center">{credit_days}</p>
                            </div> 

                            <div className="gc_cell_5-7">
                                <p className = "providerreader__label">Límite de Crédito</p>
                                <p className = "providerreader__content text-end">{credit_limit}</p>
                            </div>  

                            {/**  ADDRESS */}
                            <div className="gc_cell_1-7 subtitle">
                                <p>Dirección</p>
                            </div> 

                            {/**Row 4 */}
                            <div className="gc_cell_1-5">
                                <p className = "providerreader__label">Calle</p>
                                <p className = "providerreader__content">{ street }</p>
                            </div> 
                            <div className="gc_cell_5-6">
                                <p className = "providerreader__label">No Ext</p>
                                <p className = "providerreader__content">{num_ext}</p>
                            </div>

                            <div className="gc_cell_6-7">
                                <p className = "providerreader__label">No Int</p>
                                <p className = "providerreader__content">{num_int}</p>
                            </div>

                            {/**Row 5 */}
                            <div className="gc_cell_1-3">
                                <p className = "providerreader__label">Colonia</p>
                                <p className = "providerreader__content">{suburb}</p>
                            </div>

                            <div className="gc_cell_3-5">
                                <p className = "providerreader__label">Localidad</p>
                                <p className = "providerreader__content">{location}</p>
                            </div>

                            <div className="gc_cell_5-7">
                                <p className = "providerreader__label">Municipio</p>
                                <p className = "providerreader__content">{municipality}</p>
                            </div>

                            {/**Row 6 */}
                            <div className="gc_cell_1-4">
                                <p className = "providerreader__label">Referencia</p>
                                <p className = "providerreader__content">{reference}</p>
                            </div>

                            <div className="gc_cell_4-5">
                                <p className = "providerreader__label">Estado</p>
                                <p className = "providerreader__content">{state}</p>
                            </div>

                            <div className="gc_cell_5-6">
                                <p className = "providerreader__label">País</p>
                                <p className = "providerreader__content">{country}</p>
                            </div>

                            <div className="gc_cell_6-7">
                                <p className = "providerreader__label">C.P.</p>
                                <p className = "providerreader__content">{postalcode}</p>
                            </div>
                            
                            {
                                //#region Emails
                            }
                            <div className = "gc_cell_1-7 subtitle">
                                Correos Electrónicos                                
                            </div>

                            <div className = "gc_cell_1-3 h-small">
                                <p className = "providerreader__label_small">Nombre de Contacto</p>
                            </div>

                            <div className = "gc_cell_3-5 h-small">
                                <p className = "providerreader__label_small">Puesto</p>
                            </div>

                            <div className = "gc_cell_5-7 h-small">
                                <p className = "providerreader__label_small">Correo Electrónico</p>
                            </div>
                            {
                                provider_read.emails?
                                    provider_read.emails.map( email => {
                                        
                                        
                                        return(
                                            <>
                                            <div className = "gc_cell_1-3 h-small">
                                                <p className = "providerreader__content_row">{email.contact_email}</p>
                                            </div>

                                            <div className = "gc_cell_3-5 h-small">
                                                <p className = "providerreader__content_row">{email.stall_email}</p>
                                            </div>

                                            <div className = "gc_cell_5-7 h-small">
                                                <p className = "providerreader__content_row">{email.email_address}</p>
                                            </div>

                                            </>
                                        )
                                    })
                                : null
                            }
                            {
                                //#endregion 
                            }

                            {/** Phones*/}
                            <div className = "gc_cell_1-7 subtitle">
                                Teléfonos                                
                            </div>

                            <div className = "gc_cell_1-3 h-small">
                                <p className = "providerreader__label_small">Nombre de Contacto</p>
                            </div>

                            <div className = "gc_cell_3-5 h-small">
                                <p className = "providerreader__label_small">Puesto</p>
                            </div>

                            <div className = "gc_cell_5-7 h-small">
                                <p className = "providerreader__label_small">Número Telefónico</p>
                            </div>

                            {
                                provider_read.phonenumbers ?
                                    provider_read.phonenumbers.map( phone => {
                                        return(
                                            <>
                                                <div className = "gc_cell_1-3 h-small">
                                                    <p className = "providerreader__content_row">
                                                        {phone.contact_phone}
                                                    </p>
                                                </div>

                                                <div className = "gc_cell_3-5 h-small">
                                                    <p className = "providerreader__content_row">
                                                        {phone.stall_phone}
                                                    </p>
                                                </div>

                                                <div className = "gc_cell_5-7 h-small">
                                                    <p className = "providerreader__content_row text-center">
                                                        {phone.phonenumber} 
                                                        {phone.extension? 
                                                        <span> ext: {phone.extension}</span> 
                                                        :null}
                                                    </p>
                                                </div>
                                            </>
                                        )
                                    })
                                : null
                            }

                            <div className = "gc_cell_1-7 subtitle">
                                Bancos                                
                            </div>

                            <div className = "gc_cell_1-2 h-small">
                                <p className = "providerreader__label_small">Banco</p>
                            </div>

                            <div className = "gc_cell_2-3 h-small">
                                <p className = "providerreader__label_small">No de Cuenta</p>
                            </div>

                            <div className = "gc_cell_3-5 h-small">
                                <p className = "providerreader__label_small">CLABE</p>
                            </div>

                            <div className = "gc_cell_5-7 h-small">
                                <p className = "providerreader__label_small">Referencia</p>
                            </div>

                            {
                                provider_read.banksData?
                                    provider_read.banksData.map( bank => {
                                        return(
                                            <>
                                            <div className = "gc_cell_1-2 h-small">
                                                <p className = "providerreader__content_row ">{bank.bank_name}</p>
                                            </div>

                                            <div className = "gc_cell_2-3 h-small">
                                                <p className = "providerreader__content_row ">{bank.number_account}</p>
                                            </div>

                                            <div className = "gc_cell_3-5 h-small">
                                                <p className = "providerreader__content_row ">{bank.CLABE}</p>
                                            </div>

                                            <div className = "gc_cell_5-7 h-small">
                                                <p className = "providerreader__content_row ">{bank.payment_reference}</p>
                                            </div>

                                            </>
                                        )                                        
                                    })
                                :null
                            }

                            <div className = "gc_cell_1-7 subtitle">
                                Condiciones
                            </div>

                            {
                                provider_read.conditions? 
                                    provider_read.conditions.map( condts => {
                                        return(
                                            <div className = "gc_cell_1-7 h-small">
                                                <p className = "providerreader__content_row">
                                                    {condts}
                                                </p>
                                            </div>
                                        )
                                    })
                                :null
                            }

                        </div> {/**End provider container */}
                        
                    </div>
                : null
            }

        </Modal>
    )
}
