import React, { useEffect } from 'react';
import { FiBox, FiEdit, FiEye, FiFile, FiTag } from 'react-icons/fi';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { getTrademarkList } from '../../Redux/actions/productActions';
import { StartgetProviders } from '../../Redux/actions/providerActions';
import { Navbar } from '../Navbar';

export const ProductScreen = () => {

    const dispatch = useDispatch();
    useEffect(()=>{
        dispatch( getTrademarkList() );
        dispatch( StartgetProviders() );
    }, [dispatch]);

    return (
        <>
            <Navbar />
            <div className = "container">
                <h1>Productos</h1>

                <Link 
                    to="/products/newproduct" 
                    className = "btn btn-success me-4"
                >
                    <FiBox/> Nuevo Producto 
                </Link>

                <Link
                    to='/products/editproduct'
                    className = "btn btn-success me-4"
                >
                    <FiEdit/> Editar Producto
                </Link>

                <Link
                    to='/products/editproduct'
                    className = "btn btn-success me-4"
                >
                    <FiEye/> Visor de Productos
                </Link>

                <Link
                    to='/products/productsloader'
                    className = "btn btn-success me-4"
                >
                    <FiFile/> Cargar Archivo
                </Link>

                <Link to="/products/trademarks" className = "btn btn-success">
                    <FiTag/> Marcas
                </Link>


            </div>
            
        </>
    )
}
