export const utility = {
    PPF:  {
        min_ppf: '0.01', 
        percentage_ppf: '75',
        desc: 'Precio Por Fracción'
    },

    PPB:  {
        min_ppb: '1', 
        percentage_ppb: '40',
        desc: 'Precio Público'
    },

    PMMY: {
        min_pmmy: '3', 
        percentage_pmmy: '35',
        desc: 'Precio Medio Mayoreo'
    },

    PMY:  {
        min_pmy: '8', 
        percentage_pmy: '30',
        desc: 'Precio Mayoreo'
    },

    PIND: {
        min_pind: '15', 
        percentage_pind: '25',
        desc: 'Precio Industrial'
    },

    PDST: {
        min_pdst: '20', 
        percentage_pdst: '10',
        desc: 'Precio Distribuidor'
    },

}

export const productSchema = {
    code: '',
    model: '',
    description: '',
    cost: 0,
    trademark: '',
    provider:'',
    satcode: '',
    measuringunit: '',
    ean: '',
    stock:0,
    box:0,
    master:0,
    minimum_purchase: 1,
    location: '',
    quantitySold: 0,
    minimum: 0,
    maximum: 1,
    createAt: '',
    active: true,
    utilities: utility,
    categories: [],
    images: [],
    costHistory: [],
    properties: []
}

export const costHistoySchema = {
    cost: '',
    date: ''
};


