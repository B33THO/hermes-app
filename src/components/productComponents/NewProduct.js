import React, { useEffect, useState } from 'react'
import { FiSave } from 'react-icons/fi';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from '../../hooks/useForm';
import moment from 'moment';
import 'moment/locale/es';

import { getTrademarkList, saveProduct } from '../../Redux/actions/productActions';
import { StartgetProviders } from '../../Redux/actions/providerActions';
import { TrademarkSelect } from './TrademarkSelect';
import { Navbar } from '../Navbar';
import { ProviderSelect } from '../providerComponents/ProviderSelect';
import { ProductData } from './ProductData';
import { productSchema } from './ProducSchema';
import { ProductSatCode } from './ProductSatCode';
import { ProfitPercentages } from './ProfitPercentages';
import { SatSelector } from './SatSelector';
import { MeasuringUnitChooser } from './MeasuringUnitChooser';
import { useHistory } from 'react-router';
import Swal from 'sweetalert2';
import { isNuericValue } from '../../helpers/regex';

export const NewProduct = () => {

     
    //-----|| Principal State ||--------------------------------------------------------------|
    const [ product, handleChangeProduct, _, addValue ] = useForm(productSchema, 'UPPERCASE'); 
    
    
    const [satcodeValue, setSatcodeValue] = useState({
        _id: '',
        code: ''
    }); 

    const [unitcodeValue, setUnitcodeValue] = useState({
        _id: '',
        code: '',
        nombre: ''
    });    

    moment.locale();
    const date = moment().format('MM DD YYYY h:mm:ss a');
    const history = useHistory();
    //----------------------------------------------------------------------------------------|

    const dispatch = useDispatch();
  
    //destructuring
    const { 
        code, model, description, cost, location, minimum, maximum, minimum_purchase, ean, provider, 
        trademark, master, box, utilities

    } = product;  
    
    const { PDST, PIND, PMMY, PMY, PPB, PPF } = utilities;
    //----------------------------------------------Effects----------------|
    useEffect(() => {
        dispatch( getTrademarkList() );
        dispatch( StartgetProviders() );
    }, [dispatch]);    

    useEffect(() => {        
        addValue({
            satcode: satcodeValue._id,
            measuringunit: unitcodeValue._id
        });       
    }, [satcodeValue, unitcodeValue]);

    useEffect(() => {
        // create new history cost
        const costhistory = {
            cost,
            date
        }

        addValue({
            costHistory: [costhistory],
            createAt: date
        });
        
    }, [cost]);


    //---------------------------------------------------------------------|



    //------Selectors--------------------------------------------------------------------------
    const {providers} = useSelector(state => state.providers);
    const {trademarks} = useSelector(state => state.trademark);

    
    //------------------| S A V E |------| P R O D U C T |-------| F U N C T I O N |----------|

    const fireError = (field) => {
        Swal.fire({
            title: `Error en el campo "${field}"`,
            text: 'Por favor, introduzca un valor numérico mayor o igual a 0',
            icon: 'error'
        });
        return;
    }

    const handleSaveProduct = (e) => {
        e.preventDefault();       
        
        if( Number(cost) <= 0 ){
            Swal.fire({
                title: 'Error en el campo "Costo"',
                text: 'Ingrese un costo mayor a cero',
                icon: 'error'
            });
            return;
        }
        
        if(provider === "") {
            Swal.fire({
                title: 'Error en el campo "Proveedor"',
                text: 'Por favor, selecione el proveedor del producto',
                icon: 'error'
            });
            return;
        }
        
        if( trademark === "" ) {
            Swal.fire({
                title: 'Error en el campo "Marca"',
                text: 'Por favor, selecione la marca del producto',
                icon: 'error'
            });
            return;
        }        

        if( Number(minimum) < 0 || !isNuericValue(Number(minimum))){
            fireError('Mínimo');
            return;
        }

        if( Number(maximum) < 0 || !isNuericValue(Number(maximum))){
            fireError('Máximo');
            return;
        }

        if( Number(minimum_purchase) < 0 || !isNuericValue(Number(minimum_purchase)) ){
            fireError('Mínimo de compra');
        }
        
        if( Number(box) < 0 || !isNuericValue(Number(box))){
            fireError('Piezas por empaque');
            return;
        }

        if( Number(master) < 0 || !isNuericValue(Number(master))){
            fireError('Master');
            return;
        }

        // -----|||    Profit Percentages validations   |||--------

        //PPF
        if(PPF.min_ppf < 0 || !isNuericValue(PPF.min_ppf)){
            fireError('Mínimo de PPF');
            return;
        }
        
        if(PPF.percentage_ppf < 0 || !isNuericValue(PPF.percentage_ppf)){
            fireError('Porcentaje de PPF');
            return;
        }

        //PPB
        if(PPB.min_ppb < 0 || !isNuericValue(PPB.min_ppb)){
            fireError('Mínimo de PPB');
            return;
        }
        
        if(PPB.percentage_ppb < 0 || !isNuericValue(PPB.percentage_ppb)){
            fireError('Porcentaje de PPB');
            return;
        }
        
        //PMMY
        if(PMMY.min_pmmy < 0 || !isNuericValue(PMMY.min_pmmy)){
            fireError('Mínimo de PMMY');
            return;
        }
        
        if(PMMY.percentage_pmmy < 0 || !isNuericValue(PPB.percentage_ppb)){
            fireError('Porcentaje de PMMY');
            return;
        }

        //PMY
        if(PMY.min_pmy < 0 || !isNuericValue(PMY.min_pmy)){
            fireError('Mínimo de PMY');
            return;
        }
        
        if(PMY.percentage_pmy < 0 || !isNuericValue(PMY.percentage_pmy)){
            fireError('Porcentaje de PMY');
            return;
        }

        //PIND
        if(PIND.min_pind < 0 || !isNuericValue(PIND.min_pind)){
            fireError('Mínimo de PIND');
            return;
        }
        
        if(PIND.percentage_pind < 0 || !isNuericValue(PIND.percentage_pind)){
            fireError('Porcentaje de PIND');
            return;
        }
        
        //PDST
        if(PDST.min_pdst < 0 || !isNuericValue(PDST.min_pdst)){
            fireError('Mínimo de PDST');
            return;
        }

        if(PDST.percentage_pdst < 0 || !isNuericValue(PDST.percentage_pdst)){
            fireError('Porcentaje de PDST');
            return;
        }

        Swal.fire({
            title: 'Confirmación',
            text: '¿Desea guardar el nuevo producto?',
            icon: 'question',
            showConfirmButton: true,
            showCancelButton: true

        }).then( result => {
            if(result.isConfirmed){
                dispatch( saveProduct(product, history) );
            } else if (result.isDenied){
                Swal.fire('No se ha guardado el producto', '', 'info');
            }
        })
    }

    
    return (
        <>
            <Navbar />
            <div className = "container bg-ligh text-center">
            <h1 className = "mt-3">Registro de Producto</h1>
                <form onSubmit = { handleSaveProduct }> 

                    <ProductData 
                        code = { code }
                        model = { model }
                        description = { description }
                        cost = { cost }
                        location = { location }
                        ean = { ean }
                        minimum={ minimum }
                        maximum={ maximum }
                        minimum_purchase={ minimum_purchase }
                        master = { master }
                        box = { box }
                        handleChangeProduct = { handleChangeProduct }
                    />

                    <div className = "container-fluid grup p-4">
                        <div className = "row">
                            <div className = "col-md-7">
                                <ProviderSelect 
                                    data = {providers}
                                    value = { provider }
                                    handleChange = { handleChangeProduct }
                                />
                            </div>
                            <div className = "col-md-5">
                                <TrademarkSelect 
                                    data = {trademarks} 
                                    value = { trademark }
                                    handleChange={ handleChangeProduct } 
                                />
                            </div>
                        </div>

                        <div className = "row">
                            <div className = "col-md-6 mt-4">
                                <SatSelector 
                                    label = {'Código SAT'}
                                    placeholder = {"Haga click para seleccionar la clave"}
                                    satcodeValue = {satcodeValue.code}
                                    name = { 'satcodeModal' }
                                />
                            </div>

                            <div className = "col-md-6 mt-4">
                                <SatSelector 
                                    label = {'Unidad'}
                                    placeholder = {"Haga click para seleccionar la Unidad de Medida"}
                                    satcodeValue = {unitcodeValue.code + ' - ' + unitcodeValue.nombre}
                                    name = { 'measuringunitModal' }
                                />
                            </div>
                            
                        </div>
                    </div>

                    
                    <ProfitPercentages 
                        utilities = { utilities }
                        addValues = { addValue }
                        handleChange = { handleChangeProduct }
                        cost = { cost }
                    />
                    

                    <ProductSatCode                     
                        setValueSelected = { setSatcodeValue }
                    />

                    <MeasuringUnitChooser 
                        setValueSelected = { setUnitcodeValue }
                    />                      

                    <button 
                        className = "btn btn-primary fs-5 p-2 pe-5 ps-5 mb-3"
                        type="submit"
                        
                    >
                        <FiSave/> Guardar Producto
                    </button>
                    
                </form>

            </div>
        </>
    )
}
