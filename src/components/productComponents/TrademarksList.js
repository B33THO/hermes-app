import React, { useEffect } from 'react';
import { FiTrash2 } from 'react-icons/fi';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { deleteTrademark, getTrademarkList } from '../../Redux/actions/productActions';

export const TrademarksList = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    
    useEffect( ()=>{
        dispatch( getTrademarkList() );        
    }, [dispatch]);
    
    const handleDeleteTrademark = (id, tm) => {

        const isOk = window.confirm(`¿Está seguro de eliminar la marca: ${tm}?`);

        if(isOk){
            dispatch( deleteTrademark({id}, history) );
        }
        
    }
    
    const { trademarks } = useSelector(state => state.trademark);

    return (
        <table className = "table">
            <thead>
                <tr>
                    <th>Marcas Registradas</th>
                    <th></th>
                </tr>
            </thead>
            
            <tbody>
                {
                    trademarks?
                        trademarks.map( tm =>{
                            return (
                                <tr key = {tm._id}>
                                    <td>{tm.trademark}</td>
                                    <td>
                                        <button 
                                            className = "btn btn-danger"
                                            type = "button" 
                                            onClick = { ()=>handleDeleteTrademark(tm._id, tm.trademark) }
                                        >
                                            <FiTrash2 />
                                        </button>
                                    </td>
                                </tr>
                            )
                        })
                    :null
                }
            </tbody>
        </table>
    )
}
