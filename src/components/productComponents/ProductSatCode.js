import React, { useState } from 'react';
import Modal from 'react-modal';
import { useDispatch, useSelector } from 'react-redux';
import { closeSatcodeModal, getSatcodeList } from '../../Redux/actions/productActions';
import { FiChevronRight, FiChevronLeft, FiChevronsLeft, FiChevronsRight } from 'react-icons/fi';

const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

Modal.setAppElement('#root');



export const ProductSatCode = ({setValueSelected}) => {

    const dispatch = useDispatch();
    const [inputValue, setInputValue] = useState('');
  
    
    const {satcodeModal} = useSelector(state => state.productModals);
    const  {satcodes, totalPages, currentPage}  = useSelector( state => state.satcode);

  

    //------ Change de Page ---------------------------------------------------------------
    const handleChangePageMore = () => {      
        if(Number(currentPage) <= Number(totalPages)){
            dispatch( getSatcodeList(`${Number(currentPage) + 1}`, inputValue));
        }  
    }

    const handleChangePageLess = () => {        
        if(Number(currentPage)>=1){
            dispatch( getSatcodeList(`${currentPage - 1}`, inputValue));
        }
    }

    const gotoEndPage = () => {
        console.log(`${totalPages}`);
        dispatch( getSatcodeList(`${totalPages}`, inputValue));
    }

    const gotoStartPage = () => {
        console.log(`${totalPages}`);
        dispatch( getSatcodeList(`1`, inputValue));
    }
    //--------------------------------------------------------------------------------------|

    const getValueSelected = (_id, code) => {
        // console.log('obtener y envial valos seleccionado');
        // let val = e.target.getAttribute('name');
        const codeSelected = {_id, code}
        setValueSelected( {...codeSelected } );
        dispatch( closeSatcodeModal() );
    }

    //----------Find Code Function------------------------------------------------------------|
    const findCode = () => {
        if(inputValue.length > 3){
            dispatch( getSatcodeList("1", inputValue) );
        }
    }
   
    const afterOpenModal = () => {
    
    }
    
    const closeModal = () => {
        dispatch( closeSatcodeModal() );
    }
    
    return (
        <div className = "bg-secondary">
            <Modal 
                isOpen={satcodeModal}
                onAfterOpen={ afterOpenModal }
                onRequestClose={ closeModal }
                style={customStyles}
                contentLabel="Example Modal"
            >

                <div className = "container-fluid">
                    <h2>Clave de Productos o Servicios</h2>
                    <div className = "input-group mb-3">
                        <span className = "input-group-text">Buscar</span>
                        <input 
                            className = "form-control"                            
                            value = { inputValue }
                            onChange = { e => setInputValue(e.target.value) }
                            onKeyUp = { findCode }
                        />
                    </div>

                </div>

                <table className = "table table-hover">

                    <thead>
                        <tr>
                            <th>Código de Prod./Servicio</th>
                            <th>Descripcion</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            satcodes.map( sc => (
                                <tr 
                                    key = {sc._id}
                                    name = {'satcode'}
                                    onClick = { () => getValueSelected(sc._id, sc.id) }
                                >
                                    <td name = { sc._id }>{sc.id}</td>
                                    <td name = { sc._id }>{sc.descripcion}</td>                                    
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
                <div className = "text-center">
                    <p className = "text-muted">{`Pagina ${currentPage} de ${totalPages}`}</p>

                    <button 
                        className = "btn btn-primary me-3"
                        onClick = { gotoStartPage }
                    >
                        <FiChevronsLeft />
                    </button>

                    <button 
                        className = "btn btn-primary me-3"
                        onClick = { handleChangePageLess }
                    >
                        <FiChevronLeft />
                    </button>
                    
                    <button 
                        className = "btn btn-primary me-3"
                        onClick = { handleChangePageMore }
                    >
                        <FiChevronRight />
                    </button>

                    <button 
                        className = "btn btn-primary "
                        onClick = { gotoEndPage }
                    >
                        <FiChevronsRight />
                    </button>
                </div>

            </Modal>
        </div>
    )
}
