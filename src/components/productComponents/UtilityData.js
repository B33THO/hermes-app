import React from 'react';

export const UtilityData = ({ 
    tag, desc, name_min, name_percent, handleChange, 
    minValue, percentValue, price }
) => {


    return (
        <>
            <h5 className = "bg-secondary rounded-top text-light m-0" title = {desc}>
                { tag }
            </h5>
            
            <div className = "input-group rounded-bottom mb-2">

                <span className = "input-group-text" >
                    Mínimo
                </span>

                <input 
                    className = "form-control text-center"
                    placeholder = "Mínimo"
                    title="Cantidad mínima de compra para vender con esta utilidad"
                    name = { name_min }
                    value = { minValue }
                    onChange = { handleChange }
                />

                <span className = "input-group-text">&#37;</span>
                <input 
                    className = "form-control text-center"
                    placeholder = "Porcentaje"
                    name = { name_percent }                                        
                    value = { percentValue }
                    onChange = { handleChange }
                />

                <span className = "input-group-text">&#36;</span>
                <input 
                    className = "form-control text-center text-success bg-light fw-bold"
                    placeholder = "Precio"
                    value = {price}
                    readOnly
                />
            </div>
        </>
    )
}

// UtilityData.propTypes = {
//     index: PropTypes.number.isRequired,
//     minName: PropTypes.string.isRequired
// }