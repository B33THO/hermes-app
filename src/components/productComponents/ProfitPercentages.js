import React, { useEffect, useState } from 'react';
import { UtilityData } from './UtilityData';
import { utility } from './ProducSchema';
import { useForm } from '../../hooks/useForm';

export const ProfitPercentages = ({utilities, addValues, cost}) => {

    const tags = Object.keys( utility );

    const { PPF, PPB, PMMY, PMY, PIND, PDST } = utilities;
    
    const [ppfValue, handleppfChange] = useForm({min_ppf: PPF.min_ppf, percentage_ppf: PPF.percentage_ppf});
    const [ppbValue, handleppbChange] = useForm({min_ppb: PPB.min_ppb, percentage_ppb: PPB.percentage_ppb});
    const [pmmyValue, handlepmmyChange] = useForm({min_pmmy: PMMY.min_pmmy, percentage_pmmy: PMMY.percentage_pmmy});
    const [pmyValue, handlepmyChange] = useForm({min_pmy: PMY.min_pmy, percentage_pmy: PMY.percentage_pmy});
    const [pindValue, handlepindChange] = useForm({min_pind: PIND.min_pind, percentage_pind: PIND.percentage_pind});
    const [pdstValue, handlepdstChange] = useForm({min_pdst: PDST.min_pdst, percentage_pdst: PDST.percentage_pdst});

    const [ppfPrice, setPpfPrice] = useState(0);
    const [ppbPrice, setPpbPrice] = useState(0);
    const [pmmyPrice, setPmmyPrice] = useState(0);
    const [pmyPrice, setPmyPrice] = useState(0);
    const [pindPrice, setIndPrice] = useState(0);
    const [pdstPrice, setPdstPrice] = useState(0)

    //Destructuring
    const { min_ppf, percentage_ppf } = ppfValue;
    const { min_ppb, percentage_ppb } = ppbValue;
    const { min_pmmy, percentage_pmmy } = pmmyValue;
    const { min_pmy, percentage_pmy } = pmyValue;
    const { min_pind, percentage_pind } = pindValue;
    const { min_pdst, percentage_pdst } = pdstValue;  

    const calculatePrice = (percent) => {
        return ( (((Number( percent)/100) + 1) * cost).toFixed(4) )
    }


    useEffect( () => {

        const uts = {
            PPF: ppfValue,
            PPB: ppbValue,
            PMMY: pmmyValue,
            PMY: pmyValue,
            PIND: pindValue,
            PDST: pdstValue
        }
        addValues({

            utilities: {...utilities, ...uts}
        });

        setPpfPrice( calculatePrice( percentage_ppf ) );
        setPpbPrice( calculatePrice( percentage_ppb ));
        setPmmyPrice( calculatePrice( percentage_pmmy ));
        setPmyPrice( calculatePrice( percentage_pmy ));
        setIndPrice( calculatePrice( percentage_pind ));
        setPdstPrice( calculatePrice( percentage_pdst ));

    }, [ppfValue, ppbValue, pmmyValue, pmyValue, pindValue, pdstValue, cost]);



    return (
        <div className = "container-fluid grup p-4 text-center">
            <h3 className = "mb-4">Porcentaje de Utilidades</h3>

            <div className = "row mb-4">

                <div className = "col-md-6">
                    <UtilityData 
                        tag = { tags[0] }
                        desc= { PPF.desc }
                        name_min = { 'min_ppf' }
                        name_percent = {'percentage_ppf'}
                        minValue = { min_ppf }
                        percentValue = { percentage_ppf }
                        handleChange = { handleppfChange }
                        price = { ppfPrice }
                    />
                </div>
                
                <div className = "col-md-6">
                    <UtilityData 
                        tag = { tags[1] }
                        desc = { PPB.desc }
                        name_min = { 'min_ppb' }
                        name_percent = {'percentage_ppb'}
                        minValue = { min_ppb }
                        percentValue = { percentage_ppb }
                        handleChange = { handleppbChange }
                        price = { ppbPrice }                        
                    />
                </div>
                
                
            </div>

            <div className = "row mb-4">
                <div className = "col-md-6">
                    <UtilityData 
                        tag = { tags[2] }
                        desc = { PMMY.desc }
                        name_min = { 'min_pmmy' }
                        name_percent = { 'percentage_pmmy' }
                        minValue = { min_pmmy }
                        percentValue = { percentage_pmmy }
                        handleChange = { handlepmmyChange }
                        price = { pmmyPrice }
                    />
                </div>

                <div className = "col-md-6">
                    <UtilityData 
                        tag = { tags[3] }
                        desc = { PMY.desc}
                        minName = 'PMY'
                        name_min = { 'min_pmy' }
                        name_percent = { 'percentage_pmy' }
                        minValue = { min_pmy }
                        percentValue = { percentage_pmy }
                        handleChange = { handlepmyChange }
                        price = { pmyPrice }
                    />
                </div>

            </div>

            <div className = "row">                
                
                <div className = "col-md-6">
                    <UtilityData 
                        tag = { tags[4] }
                        desc = { PIND.desc }
                        name_min = { 'min_pind' }
                        name_percent = { 'percentage_pind' }
                        minValue = { min_pind }
                        percentValue = { percentage_pind }
                        handleChange = { handlepindChange }
                        price = { pindPrice }
                    />
                </div>
                
                <div className = "col-md-6">
                    <UtilityData 
                        tag = { tags[5] }
                        name_min = { 'min_pdst' }
                        name_percent = { 'percentage_pdst' }
                        minValue = { min_pdst }
                        percentValue = { percentage_pdst }
                        handleChange = { handlepdstChange }
                        price = { pdstPrice }
                    />
                </div>
            </div>
        </div>
    )
}
