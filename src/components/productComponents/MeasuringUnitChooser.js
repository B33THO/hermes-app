import React, { useState } from 'react';
import Modal from 'react-modal';
import { useDispatch, useSelector } from 'react-redux';
import { closeMeasuringUnitModal, getUnitCodesList } from '../../Redux/actions/productActions';
import { FiChevronRight, FiChevronLeft, FiChevronsLeft, FiChevronsRight } from 'react-icons/fi';

const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

Modal.setAppElement('#root');



export const MeasuringUnitChooser = ({setValueSelected}) => {

    const dispatch = useDispatch();
    const [inputValue, setInputValue] = useState('');
  
    
    const {unitcodeModal} = useSelector(state => state.productModals);
    const  {units, totalPages, currentPage}  = useSelector( state => state.unitcode);
  

    //------ Change Page ---------------------------------------------------------------
    const handleChangePageMore = () => {      
        if(Number(currentPage) <= Number(totalPages)){
            dispatch( getUnitCodesList(`${Number(currentPage) + 1}`, inputValue));
        }  
    }

    const handleChangePageLess = () => {        
        if(Number(currentPage)>=1){
            dispatch( getUnitCodesList(`${currentPage - 1}`, inputValue));
        }
    }

    const gotoEndPage = () => {
        console.log(`${totalPages}`);
        dispatch( getUnitCodesList(`${totalPages}`, inputValue));
    }

    const gotoStartPage = () => {
        console.log(`${totalPages}`);
        dispatch( getUnitCodesList(`1`, inputValue));
    }
    //---------------------------------------------------------------------------------------|

    const getValueSelected = (_id, code, nombre) => {
        const codeSelected = {_id, code, nombre}
        setValueSelected( {...codeSelected } );
        dispatch( closeMeasuringUnitModal() );
    }

    //----------Find Code Function------------------------------------------------------------|
    const findCode = () => {
        if(inputValue.length > 1){
            dispatch( getUnitCodesList("1", inputValue));
        }
    }
   
    const afterOpenModal = () => {
    
    }
    
    const closeModal = () => {
        dispatch( closeMeasuringUnitModal() );
    }
    
    return (
        <div className = "bg-secondary">
            <Modal 
                isOpen={unitcodeModal}
                onAfterOpen={ afterOpenModal }
                onRequestClose={ closeModal }
                style={customStyles}
                contentLabel="Example Modal"
            >

                <div className = "container-fluid">
                    <h2>Unidad de Medida</h2>
                    <div className = "input-group mb-3">
                        <span className = "input-group-text">Buscar</span>
                        <input 
                            className = "form-control"                            
                            value = { inputValue }
                            onChange = { e => setInputValue(e.target.value) }
                            onKeyUp = { findCode }
                        />
                    </div>

                </div>

                <table className = "table table-hover">

                    <thead>
                        <tr>
                            <th>Código de Prod./Servicio</th>
                            <th>Descripcion</th>
                            <th>Simbolo</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            units.map( uc => (
                                <tr 
                                    key = {uc._id}
                                    name = {'unitcode'}
                                    onClick = { () => getValueSelected(uc._id, uc.id, uc.nombre) }
                                >
                                    <td name = { uc._id }>{uc.id}</td>
                                    <td name = { uc._id }>{uc.nombre}</td>                                    
                                    <td name = { uc._id }>{uc.simbolo}</td>                                    
                                </tr>
                            ))
                        }
                    </tbody>
                </table>

                <div className = "text-center">
                    <p className = "text-muted">{`Pagina ${currentPage} de ${totalPages}`}</p>

                    <button 
                        className = "btn btn-primary me-3"
                        onClick = { gotoStartPage }
                    >
                        <FiChevronsLeft />
                    </button>

                    <button 
                        className = "btn btn-primary me-3"
                        onClick = { handleChangePageLess }
                    >
                        <FiChevronLeft />
                    </button>
                    
                    <button 
                        className = "btn btn-primary me-3"
                        onClick = { handleChangePageMore }
                    >
                        <FiChevronRight />
                    </button>

                    <button 
                        className = "btn btn-primary "
                        onClick = { gotoEndPage }
                    >
                        <FiChevronsRight />
                    </button>
                </div>

            </Modal>
        </div>
    )
}
