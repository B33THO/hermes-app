import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from '../../../hooks/useForm';
import { uploadProductsFile } from '../../../Redux/actions/productActions';
import { Navbar } from '../../Navbar';
import { ProviderSelect } from '../../providerComponents/ProviderSelect';

export const LoaderScreen = () => {

    const {providers} = useSelector(state => state.providers);
    const dispatch = useDispatch();
    const [provider, setProvider] = useForm('');
    const [filePath, setFilePath] = useState('');

    const loadFile = () => {
        dispatch( uploadProductsFile(filePath, provider.provider) );
    }

  return (
      <>
        <Navbar />

        <div className = "container " >

            <div className = "row mt-4">

                <div className = "col">
                    <ProviderSelect 
                        data = { providers }
                        value = { provider }
                        handleChange = { setProvider }
                    />                    
                </div>
            </div>

            <div className = "row mt-5">
                <div className = "col">
                    <input 
                        type={"file"}
                        className = "form-control "
                        onChange={ e => setFilePath(e.target.value) }
                    />

                </div>                

                <div className = "col-2">
                    <button className = "btn btn-primary" onClick={loadFile}>
                        Cargar
                    </button>
                </div>       

            </div>




        </div>
      </>
  );
};
