import React from 'react';
import { useDispatch } from 'react-redux';
import { openMeasuringUnitModal, openSatcodeModal } from '../../Redux/actions/productActions';

export const SatSelector = ({label, placeholder, satcodeValue, name } ) => {

    const dispatch = useDispatch();

    const openModal = (e) => {   
        
        let name = e.target.name;
        
        switch (name) {
            case 'satcodeModal':
                
                dispatch( openSatcodeModal() );
                break;
                
            case 'measuringunitModal':
                
                dispatch( openMeasuringUnitModal() );
                break;

            default:
                break;
        }
    }


    return (
        <div 
            onClick = { openModal }
            className = "input-group mb-3"
        >
            <span className = "input-group-text bg-secondary text-light">{label}</span>
            <input 
                className = "form-control text-center"
                name = { name }
                placeholder = { placeholder }
                disabled
                value = { satcodeValue }
            />
            

        </div>
    )
}
