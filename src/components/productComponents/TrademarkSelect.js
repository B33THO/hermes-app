import React from 'react';

export const TrademarkSelect = ({data, value, handleChange, select = false}) => {
    

    return (
        <select
            className="form-select" 
            aria-label="Default select example" 
            value = {value.trademark} 
            onChange= { handleChange }
            name = {'trademark'} 
            placeholder = "Seleccione una Marca"
            
        >
            <option>--Seleccione una Marca--</option>
            {
                data?
                data.map( d => {
                    return (
                        <option
                            key = {d._id}
                            value = {d._id}  
                            selected = { select === d._id}                          
                        >
                            {d.trademark}
                        </option>
                    )
                })
                :null
            }
        </select>
    )
}
