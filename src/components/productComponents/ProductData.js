import React from 'react';
import { Input } from '../controls/Input';

export const ProductData = ({ code, model, description, cost, location, ean, master, box, 
    minimum, maximum, minimum_purchase, handleChangeProduct }) => {
    
    return (
        <div className = "container-fluid grup p-4">
            <div className = "row">

                <div className = "col-md-5">
                    <Input
                        type = { 'text' }
                        name = { 'code' } 
                        label = {'Código'} 
                        placeholder = {'Ingrese un Código'} 
                        value = { code }
                        onChange = { handleChangeProduct }
                    />
                </div>

                <div className = "col-md-4">
                    <Input
                        type = { 'text' }
                        name = { 'model' } 
                        label = {'Modelo'} 
                        placeholder = {'Ingrese un modelo'} 
                        value = { model }
                        onChange = { handleChangeProduct }
                    />
                </div>

                <div className = "col-md">
                    <Input
                        type = { 'text' }
                        name = { 'cost' } 
                        label = {'Costo $'} 
                        placeholder = {'Ingrese el costo'} 
                        value = { cost }
                        onChange = { handleChangeProduct }
                    />
                </div>
            </div>

            <div className = "row">
                <div className = "col-md">
                    <Input
                        type = { 'text' }
                        name = { 'description' } 
                        label = {'Descripción'} 
                        placeholder = {'Ingrese una descripción'} 
                        value = { description }
                        onChange = { handleChangeProduct }
                    />
                </div>
            </div>

            <div className = "row mt-3">
                <div className = "col-md-5">
                    <Input
                        type = { 'text' }
                        name = { 'ean' } 
                        label = {'EAN'} 
                        placeholder = {'Ingrese código de barras'} 
                        value = { ean }
                        onChange = { handleChangeProduct }
                    />
                </div>

                <div className = "col-md">
                    <Input
                        type = { 'text' }
                        name = { 'location' } 
                        label = {'Ubicación'} 
                        placeholder = {''} 
                        value = { location }
                        onChange = { handleChangeProduct }
                    />
                </div>

                <div className = "col-md">
                    <Input
                        type = { 'text' }
                        name = { 'minimum' } 
                        label = {'Mínimo'} 
                        placeholder = {''} 
                        value = { minimum }
                        onChange = { handleChangeProduct }
                    />
                </div>

                <div className = "col-md">
                    <Input
                        type = { 'text' }
                        name = { 'maximum' } 
                        label = {'Máximo'} 
                        placeholder = {''} 
                        value = { maximum }
                        onChange = { handleChangeProduct }
                    />
                </div>
            </div>

            <div className = "row">
                <div className = "col-md">
                    <Input
                        type = { 'number' }
                        name = { 'box' } 
                        label = {'Pieza x empaque'} 
                        placeholder = {''} 
                        value = { box }
                        onChange = { handleChangeProduct }
                    />
                </div>

                <div className = "col-md">
                    <Input
                        type = { 'number' }
                        name = { 'master' } 
                        label = {'Master'} 
                        placeholder = {''} 
                        value = { master }
                        onChange = { handleChangeProduct }
                    />
                </div>

                <div className = "col-md">
                    <Input
                        type = { 'number' }
                        name = { 'minimum_purchase' } 
                        label = {'Mínimo de compra'} 
                        placeholder = {''} 
                        value = { minimum_purchase }
                        onChange = { handleChangeProduct }
                    />
                </div>
            </div>
                
            
        </div>
    )
}
