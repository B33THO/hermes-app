import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setProdutEdit } from '../../../Redux/actions/productActions';
import { ProductRow } from './ProductRow';

export const ProductsFound = ({isEmpty, setInput}) => {    

    const product = useSelector(state => state.product);
    const dispatch = useDispatch();
    
    const { products } = product;

    let classname;

    if(isEmpty){
        classname = ""
    } else {
        classname = "d-flex"
    }

    const handleSelectProduct = (productSelected) => {
        
        dispatch( setProdutEdit(productSelected) );
        setInput('');
    }

    return (
        <div className = {`products_found_container ${classname}`}>
            
            
            <ul className = "list-group w-100">

                <li className = "list-group-item bg-dark text-light">
                    <div className = "row ">
                        <div className = "col-2">Código</div>
                        <div className = "col">Modelo</div>
                        <div className = "col-4">Descripción</div>
                        <div className = "col">Marca</div>
                        <div className = "col-3">Proveedor</div>
                    </div>
                </li>

                {
                    products?
                        products.map( product => (
                            <ProductRow 
                                li_key = {product._id}
                                code = { product.code }
                                model = { product.model }
                                description={ product.description }
                                brand={ product.trademarkData['trademark'] }
                                provider={ product.providerData['provider_name']}
                                handleSelect={ ()=>handleSelectProduct(product) }
                            />
                        ))
                    :null
                }

            </ul>
        </div>
    )
}
