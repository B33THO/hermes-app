
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import { isNuericValue } from '../../../helpers/regex';
import { useForm } from '../../../hooks/useForm';
import { editProduct, findProduct } from '../../../Redux/actions/productActions';
import { Navbar } from '../../Navbar';
import { ProviderSelect } from '../../providerComponents/ProviderSelect';
import { MeasuringUnitChooser } from '../MeasuringUnitChooser';
import { productSchema } from '../ProducSchema';
import { ProductData } from '../ProductData';
import { ProductSatCode } from '../ProductSatCode';
import { ProfitPercentages } from '../ProfitPercentages';
import { SatSelector } from '../SatSelector';
import { TrademarkSelect } from '../TrademarkSelect';
import { ProductsFound } from './ProductsFound';

export const EditProduct = () => {
    
    const [ productState, handleProductChangue, _, addValue ] = useForm(productSchema, 'UPPERCASE');
    const [findValue, setFindValue] = useState('');
    const [satcodeValues, setSatcodeValues] = useState({_id:'', code:''});
    const [unitcodeValues, setUnitcodeValues]  = useState({_id:'', code:'', nombre: ''});

    const dispatch = useDispatch();
    
    //Selectors
    const { productToEdit } = useSelector( state => state.product );
    const { providers } = useSelector( state => state.providers );
    const { trademarks } = useSelector(state => state.trademark);

    const history = useHistory();

    const { code, model, description, cost, location, ean, box, master, minimum_purchase,
        minimum, maximum, utilities,
        trademark, provider,
    } = productState;

    const { PDST, PIND, PMMY, PMY, PPB, PPF } = utilities;

    useEffect( () => {

        if(Object.keys(productToEdit).length !== 0){

            setSatcodeValues({
                _id: productToEdit.satcodeData['_id'],
                code: productToEdit.satcodeData['id']
            });

            setUnitcodeValues({
                _id: productToEdit.measuringunitData['_id'],
                code: productToEdit.measuringunitData['id'],
                nombre: productToEdit.measuringunitData['nombre']
            });

            addValue({
                
                code: productToEdit.code,
                model: productToEdit.model,
                description: productToEdit.description,
                cost: Number( productToEdit.cost),
                location: productToEdit.location,
                ean: productToEdit.ean,
                box: productToEdit.box,
                master: productToEdit.master,
                minimum_purchase: productToEdit.minimum_purchase,
                minimum: productToEdit.minimum,
                maximum: productToEdit.maximum,
                provider: productToEdit.providerData['_id'],
                trademark: productToEdit.trademarkData['_id'],
                satcode: productToEdit.satcodeData['_id'],
                measuringunit: productToEdit.measuringunitData['_id'],
                utilities: productToEdit.utilities,
                costHistory: productToEdit.costHistory
            });

        }
    },[productToEdit]);

    useEffect( () => {
        addValue({
            satcode: satcodeValues._id
        })
    }, [satcodeValues]);

    useEffect( () => {
        addValue({
            measuringunit: unitcodeValues._id
        })
    }, [unitcodeValues]);

    let isEmpty = true;
    isEmpty = findValue.length < 4

    const searchProduct = () => {        
        if(findValue.length >= 4){
            dispatch( findProduct(findValue, 1) );
        }
    }
    
    // ---------- Save function ----------------||||
    const fireError = (field) => {
        Swal.fire({
            title: `Error en el campo "${field}"`,
            text: 'Por favor, introduzca un valor numérico mayor o igual a 0',
            icon: 'error'
        });
        return;
    }
 
    const handleSubmit = (e) => {

        e.preventDefault();

        if( Number(cost) <= 0) {
            Swal.fire({
                title: 'Error en el campo "Costo"!',
                text: 'Ingrese un costo mayor a cero',
                icon: 'error'
            });
            return;
        }

        if( provider === ''){
            Swal.fire({
                title: 'Error en el campo "Proveedor"',
                text: 'Por favor, selecione el proveedor del producto',
                icon: 'error'
            });
            return;
        }

        if( trademark === ''){
            Swal.fire({
                title: 'Error en el campo "Marca"',
                text: 'Por favor, selecione la marca del producto',
                icon: 'error'
            });
            return;
        }

        if( Number(minimum) < 0 || !isNuericValue(Number(minimum))){
            fireError('Mínimo');
            return;
        }

        if( Number(maximum) < 0 || !isNuericValue(Number(maximum))){
            fireError('Máximo');
            return;
        }

        if( Number(minimum_purchase) < 0 || !isNuericValue(Number(minimum_purchase)) ){
            fireError('Mínimo de compra');
        }
        
        if( Number(box) < 0 || !isNuericValue(Number(box))){
            fireError('Piezas por empaque');
            return;
        }

        if( Number(master) < 0 || !isNuericValue(Number(master))){
            fireError('Master');
            return;
        }

        // -----|||    Profit Percentages validations   |||--------

        //PPF
        if(PPF.min_ppf < 0 || !isNuericValue(PPF.min_ppf)){
            fireError('Mínimo de PPF');
            return;
        }
        
        if(PPF.percentage_ppf < 0 || !isNuericValue(PPF.percentage_ppf)){
            fireError('Porcentaje de PPF');
            return;
        }

        //PPB
        if(PPB.min_ppb < 0 || !isNuericValue(PPB.min_ppb)){
            fireError('Mínimo de PPB');
            return;
        }
        
        if(PPB.percentage_ppb < 0 || !isNuericValue(PPB.percentage_ppb)){
            fireError('Porcentaje de PPB');
            return;
        }
        
        //PMMY
        if(PMMY.min_pmmy < 0 || !isNuericValue(PMMY.min_pmmy)){
            fireError('Mínimo de PMMY');
            return;
        }
        
        if(PMMY.percentage_pmmy < 0 || !isNuericValue(PPB.percentage_ppb)){
            fireError('Porcentaje de PMMY');
            return;
        }

        //PMY
        if(PMY.min_pmy < 0 || !isNuericValue(PMY.min_pmy)){
            fireError('Mínimo de PMY');
            return;
        }
        
        if(PMY.percentage_pmy < 0 || !isNuericValue(PMY.percentage_pmy)){
            fireError('Porcentaje de PMY');
            return;
        }

        //PIND
        if(PIND.min_pind < 0 || !isNuericValue(PIND.min_pind)){
            fireError('Mínimo de PIND');
            return;
        }
        
        if(PIND.percentage_pind < 0 || !isNuericValue(PIND.percentage_pind)){
            fireError('Porcentaje de PIND');
            return;
        }
        
        //PDST
        if(PDST.min_pdst < 0 || !isNuericValue(PDST.min_pdst)){
            fireError('Mínimo de PDST');
            return;
        }

        if(PDST.percentage_pdst < 0 || !isNuericValue(PDST.percentage_pdst)){
            fireError('Porcentaje de PDST');
            return;
        }

        Swal.fire({
            title: 'Confirmación',
            text: '¿Desea guardar los cambios realizados al producto?',
            icon: 'question',
            showConfirmButton: true,
            showDenyButton: true
        }).then( result => {

            if(result.isConfirmed){

                console.log(productState);                
                dispatch( editProduct(productToEdit._id, productState, history) );

            }else if( result.isDenied){
                Swal.fire('Cambios no guardados', '', 'info');
            }
        }) 
    }
    return (
        <>
            <Navbar />

            <div className = "container">
                <h4 className = "mt-3">Editar Producto</h4> 
                <input 
                    type='text'
                    className = "form-control mt-3 bg_dark "
                    placeholder = "Ingrese código, modelo, descripción, o codigo de barras y precione enter"
                    name = "findValue"
                    id = "findValue"
                    autoComplete='off'
                    value = { findValue }
                    onChange = { e => { setFindValue(e.target.value)} }
                    onKeyUp={ searchProduct }                                        
                />   

                            

                <ProductsFound 
                    isEmpty = { isEmpty }
                    setInput = { setFindValue }
                />

                <form onSubmit={handleSubmit}>

                <ProductData 
                    code={ code }
                    model={ model }
                    description={ description }
                    cost={ cost }
                    location={ location }
                    ean={ ean }
                    master={ master }
                    box={ box }
                    minimum={ minimum }
                    maximum={ maximum }
                    minimum_purchase={ minimum_purchase }
                    handleChangeProduct={ handleProductChangue }
                />

                <div className = "container-fluid grup p-4">
                    <div className = "row text-center">
                        <div className = "col">
                            <p className = "subtitle rounded-top m-1">Proveedor</p>
                            <ProviderSelect 
                                data ={ providers }
                                value={ provider }
                                handleChange={ handleProductChangue }
                                select = { provider }
                            />
                        </div>

                        <div className = "col">
                            <p className = "subtitle rounded-top m-1">Marca</p>
                            <TrademarkSelect 
                                data={ trademarks }
                                value={ trademark }
                                handleChange={ handleProductChangue }
                                select = { trademark }
                            />
                        </div>
                    </div>
                

                    <div className = "row mt-4">
                        <div className = "col">
                            <SatSelector 
                                label = {'Código SAT'}
                                placeholder = {"Haga click para seleccionar la clave del producto"}
                                satcodeValue={ satcodeValues.code }
                                name = {'satcodeModal'}
                            />
                        </div>

                        <div className = "col">
                            <SatSelector 
                                label={'Unidad'}
                                placeholder={ 'Haga click para seleccionar la Unidad de Medida' }
                                satcodeValue={ unitcodeValues.code + ' - ' + unitcodeValues.nombre }
                                name = { 'measuringunitModal' }
                            />
                        </div>
                    </div>
                </div>

                <ProfitPercentages 
                    utilities={ utilities }
                    addValues={ addValue }
                    cost={ cost }
                />
                
                <ProductSatCode 
                    setValueSelected={ setSatcodeValues }
                />

                <MeasuringUnitChooser 
                    setValueSelected={ setUnitcodeValues }
                />

                <div className = "text-center">
                    <button
                        type='submit'
                        className = "btn btn-primary mb-3 text-center w-25"
                        disabled = {code === '' || description === ''}
                    >
                        Guardar Cambios
                    </button>

                </div>

                </form>
            </div>
        </>
    )
}
