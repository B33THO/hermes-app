import React from 'react';

export const ProductRow = ({li_key, code, model, description, brand, provider, handleSelect}) => {
    
    
    return (
        <>
            {
                li_key?
                    <li 
                        key={ li_key }
                        className = "list-group-item pointer fs_small"
                        onClick = { handleSelect }
                    >
                        <div className = "row ">

                            <div className = "col-2 border-end">
                                {code}
                            </div>

                            <div className = "col border-end">
                                {model}
                            </div>
                            
                            <div className = "col-4 border-end">
                                {description}
                            </div>

                            <div className = "col border-end">
                                {brand}
                            </div>

                            <div className = "col-3 border-end">
                                {provider}
                            </div>
                        </div>
                    </li>

                :null
            }

        </>
       
    )
}
