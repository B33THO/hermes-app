import React from 'react';
import { FiSave } from 'react-icons/fi';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { useForm } from '../../hooks/useForm';
import { saveTrademark } from '../../Redux/actions/productActions';
import { Input } from '../controls/Input';
import { Navbar } from '../Navbar';
import { TrademarksList } from './TrademarksList';

export const Trademarks = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const [ trademarkValue, handleChange ] = useForm({trademark: ''}, 'UPPERCASE');
    const {trademark} = trademarkValue;

    const handleSubmit = (e) => {
        e.preventDefault();
        if( trademark === ''){
            alert('agregue una marca');
            return;
        }

        dispatch( saveTrademark(trademarkValue, history) );
    }

    return (
        <>
            <Navbar />
            <div className = "container">

                <h1>Marcas</h1>                
                                  
                    <form onSubmit = {handleSubmit}>

                        <div className = "row">

                            <div className = "col-md-8">  
                                <Input 
                                    type = { 'text' }
                                    name = { 'trademark' }
                                    label = { 'Nueva Marca' }
                                    placeholder = { 'Ingrese Marca' }
                                    value = { trademark }
                                    onChange = { handleChange }
                                />
                            </div>    

                            <div className = "col-md-4">
                                <button type="submit" className = "btn btn-success">
                                    <FiSave/> Guardar Marca
                                </button>
                            </div>

                        </div> {/**End Row */}

                    </form>
                
                    <TrademarksList />

            </div>
        </>
    )
}
