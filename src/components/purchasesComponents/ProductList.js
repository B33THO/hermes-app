import React from 'react'
import { PurchaseProductItem } from './PurchaseProductItem'

export const ProductList = () => {
    return (
        <ul className = "list-group">
            <PurchaseProductItem />
        </ul>
    )
}
