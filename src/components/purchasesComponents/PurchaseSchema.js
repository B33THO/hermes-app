

export const productData = {
    quantity_buy: 0,
    id_product: '',
    cost: 0,
    total: 0
}

export const payment_date = {    
    date: '',
    discount: 0    
}

export const purchaseSchema = {

    invoiceId: '',
    provider: '',
    date_of_issue: '',
    expiration_date: '',
    paidoff: false,
    isCreditSale: true,
    subtotal: 0,
    iva: 0,
    total: 0,
    products:[],
    payment_date: []

}
