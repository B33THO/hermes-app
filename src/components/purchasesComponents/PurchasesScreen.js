import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { StartgetProviders } from '../../Redux/actions/providerActions'
import { Navbar } from '../Navbar'

export const PurchasesScreen = () => {

    const dispatch = useDispatch();

    useEffect( () => {
        dispatch( StartgetProviders() );
    }, [dispatch]);


    return (
        <>
            <Navbar />

            <div className = "container">

                <h1>Compras</h1>

                <Link className = "btn btn-success" to="/purchases/registerpurchase">Registrar Compra</Link>
            </div>
        </>
    )
}
