import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { FiPlusSquare, FiTrash2 } from "react-icons/fi";
import { useForm } from '../../hooks/useForm';
import { Navbar } from '../Navbar'
import { ProviderSelect } from '../providerComponents/ProviderSelect'
import { purchaseSchema } from './PurchaseSchema';
import { ProductList } from './ProductList';

export const RegisterPurchase = () => {

    
    const {providers} = useSelector(state => state.providers);
    const [purchase, handlePurchaseChange, reset, addValue] = useForm(purchaseSchema, 'UPPERCASE');
    const [isCredit, setIsCredit] = useState(true);

    const [discounts, handleDiscountChange, resetDiscount] = useForm({date:'', discount:0});

    const { 
        invoiceId, provider, date_of_issue, isCreditSale,
        subtotal, iva, total, expiration_date, payment_date 
    } = purchase;

    const handleCheckChange = () => {
        setIsCredit(!isCredit);
    }
    
    const handleAddDatePayment = () => {
        
        if( date === '' ){
            alert('Selecione una fecha de pago');
            return;
        }

        if(discount === 0){
            alert('Ingrese un porcentaje de descuento válido');
            return;
        }

        let dateExist;
        payment_date.filter( pd => {
            if(pd.date === date){
                alert('La fecha ya ha sido registrada'); 
                dateExist = true;               
                return dateExist;
            } else {
                dateExist = false;
                return dateExist;
            }
        })
        
        if(!dateExist){

            addValue({
                payment_date: [...payment_date, discounts]
            });
    
            resetDiscount();
        }
    }

    const handleDeleteDatePayment = (dateSelected) => {
        
        addValue({
            payment_date: payment_date.filter( p => p.date !== dateSelected)
        })
    }

    const { date, discount } = discounts;

    useEffect(() => {
        
        addValue({
            isCreditSale: isCredit
        })
    }, [isCredit]);

    return (
        <>
            <Navbar />            
            <div className = "container">

                <form>
                    <h1 className = "mt-3">Registro de Compra</h1>
                    <div className = "container-fluid grup p-5">

                        <h4 className = "bg-info rounded p-2 text-light text-center">
                            Datos de Factura
                        </h4>

                        <div className = "row">

                            <div className = "col-md-6">
                                <label>Proveedor</label>
                                <ProviderSelect 
                                    data = { providers }
                                    value = { provider }
                                    handleChange = { handlePurchaseChange }
                                />
                            </div>

                            <div className = "col-md-3">
                                <label htmlFor="">Folio</label>
                                <input 
                                    type= "text" 
                                    className = "form-control text-center" 
                                    placeholder="Folio de Factura"
                                    name = "invoiceId"
                                    value = { invoiceId }
                                    onChange = { handlePurchaseChange }
                                />
                            </div>


                            <div className = "col-md-3">
                                <label htmlFor="">Fecha de Emisión</label>
                                <input 
                                    type= "date" 
                                    className = "form-control text-center" 
                                    placeholder="Folio de Factura"
                                    name = "date_of_issue"
                                    value = { date_of_issue }
                                    onChange = { handlePurchaseChange }
                                />
                            </div>
                        </div> {/*end row*/}

                        <div className = "row mt-4">

                            <div className = "col-md-3">
                                <label>Subtotal</label>
                                <input 
                                    type= "text" 
                                    className = "form-control text-center" 
                                    placeholder="Folio de Factura"
                                    name = "subtotal"
                                    value = { subtotal }
                                    onChange = { handlePurchaseChange }
                                />
                            </div>

                            <div className = "col-md-3">
                                <label htmlFor="">I.V.A</label>
                                <input 
                                    type= "text" 
                                    className = "form-control text-center" 
                                    placeholder="Folio de Factura"
                                    name = "iva"
                                    value = { iva }
                                    onChange = { handlePurchaseChange }
                                />
                            </div>

                            <div className = "col-md-3">
                                <label htmlFor="">Total</label>
                                <input 
                                    type= "text" 
                                    className = "form-control text-center" 
                                    placeholder="Folio de Factura"
                                    name = "total"
                                    value = { total }
                                    onChange = { handlePurchaseChange }
                                />
                            </div>


                            <div className = "col-md-3">
                                <label htmlFor="">Fecha de Vencimiento</label>
                                <input 
                                    type= "date" 
                                    className = "form-control text-center" 
                                    placeholder="Folio de Factura"
                                    name = "expiration_date"
                                    value = { expiration_date }
                                    onChange = { handlePurchaseChange }
                                />
                            </div>

                            {/* Credit option checkbox */}
                            <div className = "row ms-2 mt-3">
                                <div className = "d-flex justify-content-end">
                                    <div className="form-check form-switch ">
                                        <input 
                                            className="form-check-input" 
                                            type="checkbox"
                                            name = "isCredit"
                                            defaultChecked = { isCreditSale }
                                            value = { isCredit }                                
                                            onChange = { handleCheckChange }                                     
                                        />
                                        <label className="form-check-label" htmlFor="">Compra a crédito</label>
                                    </div>
                                </div>
                            </div>

                            {/* Other payment dates Control */}
                            <div className = "container-fluid grup p-3 ">
                                <h4 className = "bg-info rounded p-2 text-light text-center">
                                    Descuentos Por Pagos Anticipados
                                </h4>
                                <div className = "row justify-content-between">
                                    <div className = "col-md-6">
                                        <label>Fecha de Pago</label>
                                        <input
                                            type = "date"
                                            name = "date"
                                            className = "form-control text-center"
                                            value = { date }
                                            onChange = { handleDiscountChange }
                                        />
                                    </div>
                                    <div className = "col-md-3">
                                        <label>Porcentaje de descuento</label>
                                        <input
                                            type = "text"
                                            name = "discount"
                                            className = "form-control text-center"
                                            value = { discount }
                                            onChange = { handleDiscountChange }
                                        />
                                    </div>

                                    <div className = "col-md-2 mt-4">
                                        <button
                                            type="button"
                                            className = "btn btn-success"
                                            onClick = { handleAddDatePayment }
                                        >
                                            <FiPlusSquare/> Agregar
                                        </button>
                                    </div>


                                </div>

                                {/*  Other payment dates List*/}
                                <div className = "row mt-3">
                                        <ul className = "list-group">
                                            {
                                                payment_date ?
                                                    payment_date.map( p => (
                                                        <li className = "list-group-item d-flex" key= {p.date}>

                                                            <div className = "row justify-content-between">
                                                                <div className = "col-10">
                                                                    <div className = "ms-2 me-auto">
                                                                        <div className = "fw-bold">Fecha de pago: { p.date }</div>
                                                                        Descuento: 
                                                                        <span className = "badge bg-success ms-5 ps-3 pe-3">{ p.discount }%</span>                                                            
                                                                    </div>
                                                                </div>

                                                                <div className = "col-2">
                                                                    <button 
                                                                    className = "btn fs-3" 
                                                                    type="button"
                                                                    onClick = {() => handleDeleteDatePayment( p.date ) }
                                                                    >
                                                                        <FiTrash2 />
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    ))
                                                : null
                                            }
                                            
                                        </ul>
                                    </div>
                                
                            </div> {/**End Other payment section */}

                        </div> {/*end row*/}

                        

                        <div>
                            <h4 className = "bg-info rounded p-2 text-light text-center ">
                                Productos
                            </h4>
                            <input className = "form-control mb-3"/>

                            <ProductList />
                            <ProductList />
                            <ProductList />
                        </div>
                    </div> 
                </form>

            </div>{/**Main container end */} 
                  
        </>
    )
}
