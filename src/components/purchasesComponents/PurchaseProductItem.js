import React from 'react';
import { FiTrash2 } from 'react-icons/fi';

export const PurchaseProductItem = () => {

    return (
        <li className = "list-group-item mb-2">
            <div className = "row">
                <div className = "col">
                    Cant <input className = "form-control w-75 text-center "/>
                </div>

                <div className = "col">
                    <p className = "fw-bold">Código</p>  
                    {132244}              
                </div>

                <div className = "col-5">
                    <p className = "fw-bold">Descripción</p>
                    {'Producto de prueba con un nombre que esta muy largo'}
                </div>

                <div className = "col">
                    Costo <input className = "form-control text-center"/>
                    <span className = "">
                        <small className = "text-muted">
                            Costo registrado: 
                        </small> 
                        <p className = "text-success">{120.22}</p>
                    </span>
                </div>

                <div className = "col">
                    <button className = "btn fs-2" type = "button">
                        <FiTrash2 />
                    </button>
                </div>
            </div>
        </li>
    )
}
