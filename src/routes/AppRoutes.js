import React, { useEffect } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Redirect
} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { PrivateRoute } from './PrivateRoute';
import { PublicRoute } from './PublicRoute';
import { ProviderScreen } from '../components/providerComponents/ProviderScreen';
import { NewProvider } from '../components/providerComponents/NewProvider';
import { LoginScreen } from '../components/LoginScreen';
import { MainScreen } from '../components/MainScreen';
import { ProductScreen } from '../components/productComponents/ProductScreen';
import { NewProduct } from '../components/productComponents/NewProduct';
import { startChekingSession } from '../Redux/actions/authActions';
import { Trademarks } from '../components/productComponents/Trademarks';
import { PurchasesScreen } from '../components/purchasesComponents/PurchasesScreen';
import { RegisterPurchase } from '../components/purchasesComponents/RegisterPurchase';
import { EditProviderData } from '../components/providerComponents/EditProviderData';
import { OrderScreen } from '../components/order/OrderScreen';
import { EditProduct } from '../components/productComponents/ProductEdition/EditProduct';
import { Order } from '../components/order/Order';
import { LoaderScreen } from '../components/productComponents/productsFile/LoaderScreen';

export const AppRoutes = () => {

    const dispatch = useDispatch();
    const { logged } = useSelector(state => state.auth);

    //TODO: petición al backend: VERIFICAR TOKEN 
    useEffect(() => {
        dispatch( startChekingSession() );
        
    }, [dispatch])

    return (
        <Router>
            <Switch>
                <PublicRoute exact path = "/login" component = { LoginScreen } isAuth = { logged }/>
                <PrivateRoute exact path = "/hermesaplication" component = { MainScreen } isAuth = { logged }/>

                <PrivateRoute exact path = "/providers" component = { ProviderScreen } isAuth = { logged }/>
                <PrivateRoute exact path = "/provider/newprovider" component = { NewProvider } isAuth = { logged }/>
                <PrivateRoute exact path = "/provider/editprovider" component = { EditProviderData } isAuth = { logged }/>

                <PrivateRoute exact path = "/products" component = { ProductScreen } isAuth = { logged }/>
                <PrivateRoute exact path = "/products/newproduct" component = { NewProduct } isAuth = { logged }/>
                <PrivateRoute exact path = "/products/editproduct" component = { EditProduct } isAuth = { logged }/>
                <PrivateRoute exact path = "/products/trademarks" component = { Trademarks } isAuth = { logged }/>
                <PrivateRoute exact path = "/products/productsloader" component = { LoaderScreen } isAuth = { logged }/>

                <PrivateRoute exact path = "/purchases" component = { PurchasesScreen } isAuth = { logged }/>
                <PrivateRoute exact path = "/purchases/registerpurchase" component = { RegisterPurchase } isAuth = { logged }/>
                
                <PrivateRoute exact path = "/orders" component = { OrderScreen } isAuth = { logged }/>
                <PrivateRoute exact path = "/orders/neworder" component = { Order } isAuth = { logged }/>

                <Redirect to = "/hermesaplication"/>
            </Switch>
        </Router>
    )
}
