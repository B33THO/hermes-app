import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { authReducer } from './reducers/authReducer';
import { productModalsReducer, productReducer, satcodeReducer, trademarkReducer, unitcodeReducer } from './reducers/productReducer';
import { providerReducer, providersReducer } from './reducers/providerReducer';
import { uiReducer } from './reducers/uiReducer';
import { uploadProductsFileReducer } from './reducers/uploadProductsFileReducer';

const composeEnhancers = ( typeof window !== 'undefined' &&  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const rootReducer = combineReducers({
    auth: authReducer,
    prov: providerReducer,
    providers: providersReducer,
    product: productReducer,
    trademark: trademarkReducer,
    productModals: productModalsReducer,
    satcode: satcodeReducer,
    unitcode: unitcodeReducer,
    ui: uiReducer,
    uploadFile: uploadProductsFileReducer
});


export const store = createStore(
    rootReducer,
    composeEnhancers( applyMiddleware(thunk) )
);