
import { privateFetch } from "../../helpers/privateFetch";
import { publicFetch } from "../../helpers/publicFetch";
import { type } from "../types/types";

export const startLogin = (user, password) => {

    return async( dispatch ) => {

        const resp = await publicFetch('hermesapi/login', {user, password}, 'POST' );
        const data = await resp.json();
        
        if(data.ok){
            
            const { ok, token } = data;
            localStorage.setItem('token', token);
            localStorage.setItem('token-time', new Date().getTime());
            dispatch( login(user, ok, token));            
        } else {

            let error;

            if(data.msg){
                error = data.msg

            } else {
                const {user, password} = data.errors;
                if(user){
                    error = user.msg;
                } else {
                    error = password.msg;
                }
            }
            alert(error);
        }
    }
}

export const login = (user, logged, token) => {
    
    return {
        type: type.LOGIN,
        payload: {
            user,
            logged, 
            token
        }
    }
}

export const startChekingSession = () => {
    
    return async( dispatch ) => {
        const res = await privateFetch( 'hermesapi/renewsession');
        const data = await res.json();

        if(data.ok){
            
            const { ok, newToken } = data;
            localStorage.setItem('token', newToken);
            localStorage.setItem('token-time', new Date().getTime());
            dispatch( login(data.user, ok, newToken) );

        } else {
            dispatch( checkingFailed() );
        }
    }
}

export const checkingFailed= () => ({
    type: type.AUTH_CHECKING_FAILED
});

export const logout = () => ({type: type.LOGOUT});