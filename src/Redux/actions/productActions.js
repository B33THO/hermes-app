import Swal from "sweetalert2";
import { privateFetch } from "../../helpers/privateFetch";
import { productTypes } from "../types/product_types";
import { type } from "../types/types";

export const saveProduct = (product, history) => {
    
    return async() => {

        console.log(product);

        try {
            const sendReq = await privateFetch('hermesapi/product/saveproduct', product, 'POST');
            const resp = await sendReq.json();

            console.log(resp);
            if( resp.ok){
                
                Swal.fire({
                    title: 'Correcto',
                    text: 'Producto guardado correctamente',
                    icon: 'success'
                });
                history.push('/products');

            } else {

                let { source } = resp;

                switch (source) {
                    case 'DB':
                        alert(resp.msg);
                        break;
                    case 'VALIDATOR':
                        const errKeys = Object.keys(resp.errors);
                        alert(resp.errors[errKeys[0]].msg);
                        break;
                    default:
                        break;
                }
            }

        } catch (error) {
            console.log(error);
        }
    }
}

export const findProduct = (keyword, page) => {
    
    return async(dispatch) => {

        try {
            
            const request = await privateFetch(`hermesapi/product/findproduct?find=${keyword}&page=${page}`);
            const products = await request.json();

            console.log(products);
            dispatch( setProductsfound(products) )

        } catch (error) {
            console.log(error);
        }
    }
}

const setProductsfound = (productData) => {
    
    return {
        type: productTypes.SET_PRODUCTS_FOUND,
        payload: productData
    }
}

export const setProdutEdit = (productSelected) => {

    return {
        type: productTypes.SET_PRODUCT_TO_EDIT,
        payload: productSelected
    }
}

export const editProduct = (id, product, history) => {
    
    console.log(product);
    return async() => {
        try {
            const req = await privateFetch(`hermesapi/product/updateproduct/${id}`, product, 'POST');
            const resp = await req.json();

            if(resp.ok){
                Swal.fire({
                    title: 'Correcto!',
                    text: 'Producto actualizado correctamente!',
                    icon: 'success'
                });

                history.push('/products');
            }

        } catch (error) {
            console.log(error);
        }
    }
}

export const uploadProductsFile = (path, provider) => {
    
    return async() => {
        try {
            const req = await privateFetch(
                'hermesapi/product/loadproductsfromexcel', 
                {path, provider}, 
                'POST'
            );
            const resp = await req.json();

            console.log(resp);
        } catch (error) {
            console.log(error);
        }
    }
}

export const saveTrademark = (newTrademark, history) => {
    
    return async(dispatch) => {

        try {
            
            const request = await privateFetch('hermesapi/product/trademark/save', newTrademark, 'POST');
            const resp = await request.json();
            
            if(resp.ok){
                alert('Marca Guardada Correctamente');
                history.push('/products/trademarks');
            } else {
                let { source } = resp;
    
                switch (source) {
                    case 'DB':
                        alert(resp.msg)
                        break;
                
                    default:
                        break;
                }
            }

        } catch (error) {
            console.log(error);
        }
        
    }
}

export const deleteTrademark = (id, history) => {    

    return async(dispatch) => {
        const request = await privateFetch(`hermesapi/product/trademark/delete/`, id, 'DELETE');
        const resp = await request.json();
        console.log(resp);
        dispatch(getTrademarkList());
        history.push('/products/trademarks');
    }
}

export const getTrademarkList = () => {
    
    return async(dispatch) => {
        const request = await privateFetch('hermesapi/product/trademarks/list');
        const trademarks = await request.json();
        
        dispatch(setTrademarkList(trademarks.trademarklist));
    }
}

const setTrademarkList = (trademarks) => {
    return {
        type: type.SET_TRADEMARK_LIST,
        payload: trademarks
    }
}

export const getSatcodeList = (page = 1, keyword='') => {
    
    return async(dispatch) => {
        
        try {
            const request = await privateFetch(`hermesapi/product/satcode/list?page=${page}&find=${keyword}`);
            const satcodesResult = await request.json();
            dispatch( setSatcodeList(satcodesResult));
            
        } catch (error) {
            console.log(error);
        }
    }
}

export const setSatcodeList = (satcodeData) => {
    
    return {
        type: type.SET_SATCODES,
        payload: satcodeData
    }
}

export const getUnitCodesList = (page = 1, keyword = '') => {
    
    return async(dispatch) => {
        try {
            const request = await privateFetch(`hermesapi/product/measuringunits/list?page=${page}&find=${keyword}`);
            const unitcodesResult = await request.json();
            dispatch( setUnitcodeList(unitcodesResult) );

        } catch (error) {
            console.log(error);
        }
    }
}

export const setUnitcodeList = (unitcodeData) => {
    
    return {
        type: type.SET_UNITCODES,
        payload: unitcodeData
    }
}

export const openSatcodeModal = () => ({type: type.SET_OPEN_SATCODE_MODAL});
export const closeSatcodeModal = () => ({type: type.SET_CLOSE_SATCODE_MODAL});
export const openMeasuringUnitModal = () => ({type: type.SET_OPEN_UNITCODE_MODAL});
export const closeMeasuringUnitModal = () => ({type: type.SET_CLOSE_UNITCODE_MODAL});