
import Swal from "sweetalert2";
import { privateFetch } from "../../helpers/privateFetch"
import { providerTypes } from "../types/provider_types";
import { type } from "../types/types"

export const saveProvider = (newProvider, history) => {

    return async(dispatch) => {
        
        try {
            
            const req = await privateFetch('hermesapi/provider/new', newProvider, 'POST');
            const resp = await req.json();  

            if(resp.ok){
                dispatch( setProviderSaveSuccess() );
                Swal.fire({
                    title: 'Proveedor Guardado...',
                    text: 'El proveedor se a guardado correctamente',
                    icon: 'success',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                      },
                      hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                      }
                });
                history.push('/providers');

            } else { 
                
                dispatch( providerSaveError());                
                let {source} = resp;

                switch (source) {
                    case 'DB':
                        Swal.fire({
                            title: 'Error al intentar guardar...',
                            text: `${resp.msg}`,
                            icon: 'error'
                        });                        
                        break;

                    case 'VALIDATOR':

                        const errKeys = Object.keys(resp.errors);
                        Swal.fire({
                            title: 'Error de Validación',
                            text: `${resp.errors[errKeys[0]].msg}`
                        });                        
                        break;
                
                    default:
                        break;
                }
            }
            
        } catch (error) {
            console.log(error);
            return false;
        }        
    }
}

export const StartgetProviders = () => {
    
    return async(dispatch) => {

        try {
            const req = await privateFetch('hermesapi/provider/getproviders');
            const {ok, providers} = await req.json();
             
            if(ok){

                dispatch( setProviders(providers) );
                dispatch( providersIsLoading(false) );
                return providers;
            }

        } catch (error) {
            console.log(error);
        }
    }
}

export const getProviderById = (id) => {
    
    return async(dispatch) => {

        try {
            const provider = await privateFetch(`hermesapi/provider/getprovider/${id}`);
            const {providerFound} = await provider.json();

            dispatch( setProviderFound(providerFound) );
        } catch (error) {
            
        }
    }
}

const setProviders = (providers) => {
    return {
        type: type.SET_PROVIDERS,
        payload: providers
    }
}

const setProviderFound = (provider) => {
    return {
        type: type.SEARCH_PROVIDER_BY_ID,
        payload: provider
    }
}


export const setProviderSaveSuccess = () => {
    return {
        type: type.PROVIDER_SAVE_SUCCESS,
        payload: true
    }
}

export const providerSaveError = () => ({
    type: type.PROVIDER_SAVE_ERROR,
    payload: false
})


export const providersIsLoading = (finish) => ({
    type: providerTypes.PROVIDERS_LOADING,
    payload: finish
});
export const resetProvaiderData = () => ({type: type.RESET_PROVIDER_DATA});


//--Provider Data Edition---

export const selectProviderToEdit = (provider) => {
    
    return {
        type: providerTypes.SELECT_PROVIDER_TO_EDIT,
        payload: provider
    }
}

export const startEditProvider = (id, provider, history) => {
    return async(dispatch) => {
        try {
            const resp = await privateFetch(`hermesapi/provider/updateprovider/${id}`, provider, 'PUT');
            const result = await resp.json();
            const { ok } = result;
            
            if(ok){
                Swal.fire({
                    title: '¡Correcto!',
                    text: result.msg,
                    icon: 'success'
                });
                history.push('/providers');
                dispatch( editProviderSuccess() );
            } else {

                let {source} = result;

                switch (source) {
                    case 'DB':
                        Swal.fire({
                            title: 'Error al intentar guardar...',
                            text: `${resp.msg}`,
                            icon: 'error'
                        });                        
                        break;

                    case 'VALIDATOR':

                        const errKeys = Object.keys(resp.errors);
                        Swal.fire({
                            title: 'Error de Validación',
                            text: `${resp.errors[errKeys[0]].msg}`,
                            icon: 'error'
                        });                        
                        break;
                
                    default:
                        break;
                }
            }


        } catch (error) {
            console.log(error);
            Swal.fire({
                title: 'Error al intentar guardar...',
                text: `${error}`,
                icon: 'error'
            }); 
        }

    }
}

const editProviderSuccess = () => ({type: providerTypes.EDIT_PROVIDER_SUCCESS});

export const setProvidertoRead = (provider) => {
    return {
        type: providerTypes.SELECT_PROVIDER_TO_READ,
        payload: provider
    }
}