import { providerTypes } from "../types/provider_types";

export const openProviderReaderModal = () => ({type: providerTypes.OPEN_PROVIDER_READER_MODAL});
export const closeProviderReaderModal = () => ({type: providerTypes.CLOSE_PROVIDER_READER_MODAL});