import { type } from "../types/types";

const initialState = {
    logged: false,
    user: '',
    token: '',

}

export const authReducer = (state = initialState, action) => {
    
    switch (action.type) {
        
        case type.LOGIN:
            return {
                ...action.payload
            }

        case type.LOGOUT:
            return {
                ...initialState
            }

        case type.AUTH_CHECKING_FAILED:
            return {
                ...state,
                logged: false
            }
    
        default:
            return state;
    }
}