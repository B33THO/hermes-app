import { productTypes } from "../types/product_types";

const initialSate = {
    path: '',
    provider: ''
}

export const uploadProductsFileReducer = (state = initialSate, action) => {
    
    switch (action.type) {
        case productTypes.SET_FILE_DATA:
            console.log(action);
            return {
                ...state,
                ...action.payload
            }
            
    
        default:
            return state;
    }
}