import { providerTypes } from "../types/provider_types";

const ui = {
    providerReaderModal: false
}

export const uiReducer = (state = ui, action) => {
    
    switch (action.type) {

        case providerTypes.OPEN_PROVIDER_READER_MODAL:
            return {
                ...state,
                providerReaderModal: true
            }
        case providerTypes.CLOSE_PROVIDER_READER_MODAL:
            return {
                ...state,
                providerReaderModal: false
            }
    
        default:
            return state;
    }
}