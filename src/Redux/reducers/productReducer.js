
import { productTypes } from "../types/product_types";
import { type } from "../types/types";

const products = {
    products: [],
    totalPages: 1,
    currentPage: 1,
    productToEdit: {},
    productToDelete: {}
}

export const productReducer = (state = products, action) => {
    
    switch (action.type) {
        case productTypes.SET_PRODUCTS_FOUND:
            
            return {
                ...state,
                ...action.payload
            };

        case productTypes.SET_PRODUCT_TO_EDIT:

            return {
                ...state,
                productToEdit: action.payload
            }
    
        default:
            return state;
    }
}


const trademarks = [];
export const trademarkReducer = (state = trademarks, action) => {
    
    switch (action.type) {
        case type.SET_TRADEMARK_LIST:
            return {
                ...state,
                trademarks: action.payload
            }
    
        default:
            return state;
    }
}

const satcodeState = {
    totalPages: 1,
    currentPage: 1,
    keyword: '',
    satcodes: []
};
export const satcodeReducer = (state = satcodeState, action) => {
    
    switch (action.type) {
        case type.SET_SATCODES:
            
            return {
                ...state,
                ...action.payload
            }
    
        default:
            return state;
    }
}

const unitcodeState = {
    totalPages: 1,
    currentPage: 1,
    keyword: '',
    units: []
};
export const unitcodeReducer = (state = unitcodeState, action) => {
    
    switch (action.type) {
        case type.SET_UNITCODES:
            
            return {
                ...state,
                ...action.payload
            }
    
        default:
            return state;
    }
}

const productModalIsOpen = {
    satcodeModal: false,
    unitcodeModal: false
};

export const productModalsReducer = (state = productModalIsOpen, action) => {
    
    switch (action.type) {
        case type.SET_OPEN_SATCODE_MODAL:
            
            return {
                ...state,
                satcodeModal: true
            }
        case type.SET_CLOSE_SATCODE_MODAL:
        
            return {
                ...state,
                satcodeModal: false
            }
        case type.SET_OPEN_UNITCODE_MODAL:
        
            return {
                ...state,
                unitcodeModal: true
            }
        case type.SET_CLOSE_UNITCODE_MODAL:
        
            return {
                ...state,
                unitcodeModal: false
            }
    
        default:
            return state;
    }
}

