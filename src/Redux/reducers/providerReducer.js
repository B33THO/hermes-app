import { providerSchema } from "../../components/providerComponents/newProviderSchema";
import { providerTypes } from "../types/provider_types";
import { type } from "../types/types";

export const providerReducer = (state = providerSchema, action) => {
    
    switch (action.type) {           
        
        case type.SEARCH_PROVIDER_BY_ID:
            return {                
                ...action.payload
            }
        case type.RESET_PROVIDER_DATA:
            return {
                ...providerSchema
            }
        case type.PROVIDER_SAVE_SUCCESS:
            return {
                ...state,
                isSaved: true
            }
        case type.PROVIDER_SAVE_ERROR:
            return {
                ...state,
                isSaved: false
            }
    
        default:
            return state;
    }
}

const initialState = {
    loading: true,
    providers: [],
    provider_edit: null,
    provider_delete: null,
    provider_read: null
}
export const providersReducer = (state = initialState, action) => {
    
    switch (action.type) {

        case providerTypes.PROVIDERS_LOADING:
        case type.PROVIDER_LOAD_FINISHED:
            return {
                ...state,
                loading: action.payload
            }
        case type.SET_PROVIDERS:
            return {
                ...state,
                providers: action.payload
            }  
        case providerTypes.SELECT_PROVIDER_TO_EDIT:
            return {
                ...state,
                provider_edit: action.payload
            }

        case providerTypes.EDIT_PROVIDER_SUCCESS:
            return {
                ...state,
                provider_edit: null
            }
        
        case providerTypes.SELECT_PROVIDER_TO_READ:
            return {
                ...state,
                provider_read: action.payload
            }
        default:
            return state;
    }
}