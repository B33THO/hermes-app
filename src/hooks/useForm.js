import { useState } from "react";

export const useForm = (initialState = {}, caseType = "NORMAL") => {
    
    const [values, setValues] = useState(initialState);

    const handleChange = (e) => { 
        
        if(caseType === "UPPERCASE"){
            setValues({
                ...values,
                [e.target.name]: e.target.value.toUpperCase()
            });
        }else {
            setValues({
                ...values,
                [e.target.name]: e.target.value
            });
        }

    }

    const reset = ( resetValues = initialState) => {
        setValues( resetValues);
    }

    const addValue = (value) => {
        setValues({
            ...values,
            ...value
        })
    }

    return [values, handleChange, reset, addValue];

}
