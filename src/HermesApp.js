import React from 'react';
import { Provider } from 'react-redux';
import { store } from './Redux/store';
import { AppRoutes } from './routes/AppRoutes';

export const HermesApp = () => {
    
    return (
        <Provider store = { store }>
            
            <AppRoutes />

        </Provider>
    )
}
