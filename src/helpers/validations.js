export const isDuplicatePhoneRecord = (array, values) => {

    const { phonenumber, extension } = values;
    
    const isDuplicatePhone = array.find( elem => elem.phonenumber === phonenumber );
    const isDuplicateExtension = array.find( elem => elem.extension === extension );

    if( isDuplicatePhone && isDuplicateExtension){
        return true
    } else {
        return false;
    }
}

export const isEmptyPhoneValues = (phoneValues) => {
    
    const { contact_phone, stall_phone, phonenumber } = phoneValues;

    if( contact_phone === '' || stall_phone === '' || phonenumber === ''){
        return true
    } else {
        return false
    }
}

export const isDuplicateEmailRecord = (array, email) => {
    
    const isDuplicate = array.find( emails => emails.email_address === email);
    
    if(isDuplicate) {
        return true
    } else {
        return false;
    }
}

export const isEmptyEmailValues = (emailValues) => {
    
    const { contact_email, stall_email, email_address } = emailValues;

    if( contact_email === '', stall_email === '', email_address === ''){
        return true;
    } else {
        return false;
    }
}

export const isEmptyBankValues = (bankValues) => {
    
    const { bank_name, number_account, CLABE, payment_reference } = bankValues;

    if( bank_name === '' || number_account === '' || CLABE === '' || payment_reference === ''){
        return true;
    } else {
        return false;
    }
}

export const isDuplicateBankAccount = (array, number_account) => {
    
    const isDuplicate = array.find( elem => elem.number_account === number_account );

    if(isDuplicate){
        return true;
    } else {
        return false;
    }
}

export const isDuplicateCondition = (array, condition) => {
    
    const isDuplicate = array.find( elem => elem === condition);
    
    if(isDuplicate){
        return true;
    } else {
        return false;
    }
}

export const isMinLegth = (value, length) => {
    
    if(value.length >= length){
        return true;
    } else {
        return false;
    }
}