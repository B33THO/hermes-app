
const BASE_URL = process.env.REACT_APP_API_SERVER_HOST;


export const privateFetch = (endpoint, data, method = 'GET') => {
    
    const url = `${BASE_URL}/${endpoint}`;
    const token = localStorage.getItem('token') || '';

    try {
        
        if(method === 'GET'){
            
            return fetch(url, {
                method,
                headers: {
                    'Content-type': 'application-json',
                    'x-token': token
                }
            });
            
        } else  {
            
            return fetch(url, {
                method,
                headers: {
                    'Content-type': 'application/json',
                    'x-token': token
                },
                body: JSON.stringify( data )
            })
        }

    } catch (error) {
        console.log(error);
    }    

}

