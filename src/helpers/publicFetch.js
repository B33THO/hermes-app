
const BASE_URL = process.env.REACT_APP_API_SERVER_HOST;

export const publicFetch = (endpoint, data, method = 'GET') => {
    
    const url = `${BASE_URL}/${endpoint}`;
    
    try {
        
        if(method === 'GET'){
            
            return fetch(url);

        } else  {
            
            return fetch(url, {
                method,
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify( data )
            })
        }

    } catch (error) {
        console.log(error);
    }    

}