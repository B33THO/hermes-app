
export const isNuericValue = (value) => {
    
    const isNumeric = new RegExp(/^[0-9]\d*(\.\d+)?$/, 'i');
    return isNumeric.test(value);
}

export const isAlfanumericValue = (value) => {
    
    const isAlfanumeric = new RegExp(/^[a-zA-Z0-9_]+$/, 'i');
    return isAlfanumeric.test(value);
}

export const isEmail = (email) => {
    
    const isEmail = new RegExp(/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/, 'i');
    return isEmail.test(email);
}

export const isPhoneNumber = (phone) => {
    
    const isPhone = new RegExp(/^\d{10}$/, 'gi');
    return isPhone.test(phone);
}

export const isClabe = (clabe) => {
    
    const isClabe = new RegExp(/^\d{18}$/, 'gi');
    return isClabe.test(clabe);
}

export const isRfc = (rfc) => {
    
    const isRfc = new RegExp(/^[A-Z]{4}[0-9]{6}[A-Z0-9]{3}/, 'i');
    return isRfc.test(rfc);
}
